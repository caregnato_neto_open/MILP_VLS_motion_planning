classdef scenarioClass < handle
    
    
    properties
        polytopes = [];
        max_mission_time = [];
        number_obs = [];
        number_tar = [];
        number_agents = [];
        nx = [];
        nu = [];
        ny = [];
        agents = [];
        base = [];
        
    end
    
    
    methods
        
        function obj = scenarioClass(scenario)
            
            obj.polytopes = scenario.polytopes;
            obj.number_obs = scenario.number_obs;
            obj.number_tar = scenario.number_tar;
            obj.number_agents = scenario.number_agents;
            obj.base = scenario.base;
            obj.agents = scenario.agents;
        end
        
        function Plot(obj)
            
            % Colors
            agt{1}.color = 'blue'; % base
            agt{obj.number_agents}.color = 'red'; % leader
            for i = 2 : obj.number_agents-1; agt{i}.color = 'green'; end % followers
            
            
            % Plot operational region
            figure(); hold on;
            field = Polyhedron(obj.polytopes.op_region.P,obj.polytopes.op_region.q);
            field.plot('color','white','alpha',0.2)
            % plot(Polyhedron(obj.polytopes.op_region.P,obj.polytopes.op_region.q),'color','white','alpha',1);
            
            % Plot obstacles
            for i=1:obj.number_obs
                plot(Polyhedron(obj.polytopes.obs{i}.P,obj.polytopes.obs{i}.q),'color','black');
            end
            
            % Plot targets
            for i=1:obj.number_tar
                plot(Polyhedron(obj.polytopes.tar{i}.P,obj.polytopes.tar{i}.q),'color','red');
            end
            
            % Initial positions
            for i = 1 : obj.number_agents
                q = [obj.agents{i}.x0(1);
                    obj.agents{i}.x0(3);
                    -obj.agents{i}.x0(1);
                    -obj.agents{i}.x0(3)]+...
                    [obj.polytopes.agent_body{i}.q(1)/2;
                    obj.polytopes.agent_body{i}.q(2)/2;
                    +obj.polytopes.agent_body{i}.q(3)/2;
                    +obj.polytopes.agent_body{i}.q(4)/2];
                plot(obj.agents{i}.x0(1),obj.agents{i}.x0(3),'o')
                plot(Polyhedron(obj.polytopes.agent_body{i}.P,q),'color',agt{i}.color);
                
            end
            
            
            
        end
        
        
        
    end
    
    
end