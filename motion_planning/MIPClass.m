classdef MIPClass < handle
    % MIP class
    
    properties
        trans_dof = [];
        number_tar = [];
        number_obs = [];
        number_robots = [];
        number_obs_sides = [];
        number_body_sides = [];
        number_angles = [];
        number_cone_sides = [];
        N = [];
        BigM = [];
        opt_vars = [];
        constraints = [];
        index = [];
        cost = []
        gamma_incr = [];
        gamma_a = [];
        beta = [];
        base = [];
        B_obs_base = [];
        Ts = []
        
    end
    
    methods
        function MIP = MIPClass(scenario)
            
            agents = scenario.agents;
            poly = scenario.polytopes;
            base = scenario.base;
            
            % Auxiliary vars
            MIP.trans_dof = 2;
            MIP.number_robots = length(agents);
            MIP.number_obs = length(poly.obs);
            MIP.number_tar = length(poly.tar);
            MIP.number_obs_sides = size(poly.obs{1}.P,1);
            MIP.number_body_sides = size(poly.agent_body{1}.P,1);
            MIP.number_cone_sides = size(poly.fov_cones{1}.P,1);
            
            % Base vars
            MIP.base.pos = base.pos;
            MIP.base.orient_index = base.orient_index;
            
            for n = 1 : MIP.number_obs;  MIP.B_obs_base{n} = -poly.obs{n}.P*MIP.base.pos <= -poly.obs{n}.q; end

            % Optimization parameters
            MIP.N =  8;
            MIP.BigM = 100;
            MIP.gamma_incr = 0.1; 
            MIP.gamma_a = 0.1; 
            n_los_points = 4; MIP.beta = [1/(n_los_points+1):1/(n_los_points+1):1-(1/(n_los_points+1))];
            MIP.number_angles = length(scenario.polytopes.fov_cones); % number of line segments that divide the trig circle
            MIP.Ts = 4; % sampling period
            
            % Real-valued optimization variables
            MIP.opt_vars.pos = sdpvar((MIP.N+1)*MIP.number_robots*MIP.trans_dof,1);
            MIP.opt_vars.lin_vel = sdpvar((MIP.N+1)*MIP.number_robots,1);
            MIP.opt_vars.acc = sdpvar((MIP.N+1)*MIP.number_robots,1);
            MIP.opt_vars.abs_acc = sdpvar((MIP.N+1)*MIP.number_robots,1);
            MIP.opt_vars.abs_orient_diff = sdpvar((MIP.N)*(MIP.number_robots),1);   
          
            % Binary variables
            MIP.opt_vars.B_hor = binvar(MIP.N+1,1);
            MIP.opt_vars.B_tar = binvar((MIP.N+1)*MIP.number_tar,1);
            MIP.opt_vars.B_obs = binvar((MIP.N+1)*MIP.number_robots*MIP.number_obs*MIP.number_obs_sides,1);
            MIP.opt_vars.B_col = binvar((MIP.N+1)*sum(1:MIP.number_robots-1)*MIP.number_body_sides,1);
            MIP.opt_vars.B_orient = binvar(MIP.number_angles*(MIP.N+1)*(MIP.number_robots),1);
            MIP.opt_vars.B_con = binvar(2*(MIP.N+1)*sum(1:MIP.number_robots-1),1);
            MIP.opt_vars.B_con_base = binvar((MIP.N+1)*MIP.number_robots,1);
            MIP.opt_vars.B_orient_pen_1 = binvar(MIP.N*MIP.number_robots,1);
            MIP.opt_vars.B_orient_pen_2 = binvar(MIP.N*MIP.number_robots,1);
            MIP.opt_vars.B_old_los = binvar(MIP.number_obs_sides*(length(MIP.beta)-1)*(MIP.N+1)*MIP.number_obs*sum(1:MIP.number_robots-1),1);
            MIP.opt_vars.B_old_los_base = binvar(MIP.number_obs_sides*(length(MIP.beta)-1)*(MIP.N+1)*MIP.number_obs*MIP.number_robots,1);
            MIP.opt_vars.B_front_base = binvar(MIP.number_angles*MIP.number_cone_sides*(MIP.N+1)*(MIP.number_robots-1),1);
            MIP.opt_vars.B_front = binvar(MIP.number_angles*MIP.number_cone_sides*(MIP.N+1)*sum(MIP.number_robots-1),1);
            
        end
        
        function BuildOptVarIndexes(MIP)
            
            counter = 0;
            counter_obs = 0;
            counter_col = 0;
            counter_orient = 0;
            counter_tar = 0;
            counter_con = 0;
            counter_fov = 0;
            counter_B_los_base  = 0;
            counter_los_old = 0;
            counter_los_old_base = 0;
            counter_abs_orient_diff = 0;
            counter_b_front_base = 0;
            counter_b_front = 0;
            
            % B_front_base
            for i = 1 : MIP.number_robots-1
                for k = 1 : MIP.N + 1
                    for n = 1 : MIP.number_angles
                        for z = 1 : MIP.number_cone_sides
                            counter_b_front_base = counter_b_front_base + 1;
                            MIP.index.B_front_base(z,n,k,i) = counter_b_front_base;
                        end
                    end
                end
            end
            
            % B_front
            for i = 1 : MIP.number_robots-1 % Leader is not considered so -2
                for j  = 1 : MIP.number_robots-1
                    if i ~= j
                        for k = 1 : MIP.N + 1
                            for n = 1 : MIP.number_angles
                                for z = 1 : MIP.number_cone_sides
                                    counter_b_front= counter_b_front + 1;
                                    MIP.index.B_front(z,n,k,i,j) = counter_b_front;
                                end
                            end
                        end
                    end
                end
            end

            
            for i = 1 : MIP.number_robots
                for k = 1 : MIP.N+1
                    
                    counter = counter + 1;
                    
                    % linear velocity (nu) --------------------------------
                    MIP.index.lin_vel(k,i) = counter;
                    
                    % base_con --------------------------------------------
                    MIP.index.base_con(k,i) = counter;
                    
                    % pos -------------------------------------------------
                    MIP.index.pos(:,k,i) = (counter-1)*MIP.trans_dof+1:counter*MIP.trans_dof;
                    
                    % old los const ---------------------------------------
                    if i <= MIP.number_robots-1
                        for j = i + 1 : MIP.number_robots
                            for n = 1 : MIP.number_obs
                                for z = 1 : length(MIP.beta)-1
                                    for g = 1 : MIP.number_obs_sides
                                        counter_los_old = counter_los_old + 1;
                                        MIP.index.B_old_los(g,z,n,k,i,j) = counter_los_old;
                                    end
                                end
                            end
                            
                        end
                    end
                    
                    % old los const base ----------------------------------
                    for n = 1 : MIP.number_obs
                        for z = 1 : length(MIP.beta)-1
                            for g = 1 : MIP.number_obs_sides
                                counter_los_old_base = counter_los_old_base + 1;
                                MIP.index.B_old_los_base(g,z,n,k,i) = counter_los_old_base;
                            end
                        end
                    end
                            
                    % base_los --------------------------------------------
                    for z = 1 : length(MIP.beta)
                        for n = 1 : MIP.number_obs
                        counter_B_los_base = counter_B_los_base + 1;
                            MIP.index.B_los_base(i,z,k,n) = counter_B_los_base;
                        end
                    end
                    
                    % obs bin ---------------------------------------------
                    for j = 1 : MIP.number_obs
                        for n = 1 : MIP.number_obs_sides
                            counter_obs =  counter_obs + 1;
                            MIP.index.B_obs(n,k,i,j) = counter_obs; 
                        end
                    end
                    
                    % col bin ---------------------------------------------
                    if i <= MIP.number_robots-1
                        for j = i + 1 : MIP.number_robots
                            for m = 1 : MIP.number_body_sides
                                counter_col = counter_col + 1;
                                MIP.index.B_col(m,k,i,j) = counter_col;
                            end
                        end
                    end
                    
                    % orient bin ------------------------------------------
                    for p = 1 : MIP.number_angles
                        counter_orient = counter_orient+1;
                        MIP.index.B_orient(k,i,p) = counter_orient;
                        MIP.index.alpha(k,i,p) = counter_orient;
                        
                     %   if i <= MIP.number_robots-1
                            for s = i + 1 : MIP.number_robots
                                counter_fov = counter_fov + 1;
                                MIP.index.B_fov(k,i,s,p) = counter_fov;
                            end
                    %    end
                        
                    end
                    
                    % con bin ---------------------------------------------
                    if i <= MIP.number_robots-1
                        for j = i + 1 : MIP.number_robots
                            counter_con = counter_con + 1;
                            MIP.index.B_con(k,i,j) = counter_con;
                            MIP.index.B_con(k,j,i) = counter_con+1;
                            counter_con = counter_con + 1;
                        end
                    end
                    
                    
                end
            end

            for k = 1 : MIP.N+1
                % target bin b_los ----------------------------------------
                for j = 1 : MIP.number_tar
                    counter_tar = counter_tar + 1;
                    MIP.index.B_tar(k,j) = counter_tar;
                end
                
                % Orient increment ----------------------------------------
                for i = 1 : MIP.number_robots
                    counter_abs_orient_diff = counter_abs_orient_diff + 1;
                    MIP.index.abs_orient_diff(k,i) = counter_abs_orient_diff;
                end
                
            end
            
        end
       
        % Eq. (3) and (4). and const. (8), (9), (10), (11) and (12)
        function BuildNonlinearDynamicsConstraints(MIP,agents)
            
            angles = [0:2*pi/MIP.number_angles:2*pi];
            
            MIP.constraints.dyn = [];
            MIP.constraints.initial_condition = [];
            for i = 1 : MIP.number_robots
                
                
                % Initial  conditions
                initial_robot_pos = [agents{i}.x0(1);agents{i}.x0(3)];
                MIP.constraints.initial_condition = [MIP.constraints.initial_condition,
                    MIP.opt_vars.pos(MIP.index.pos(:,1,i)) == initial_robot_pos,
                    MIP.opt_vars.lin_vel(MIP.index.lin_vel(1,i)) == 0,
                    ];
                
                for k = 1 : MIP.N
                    
                    % var assignments for better readibility
                    rxk = MIP.opt_vars.pos(MIP.index.pos(1,k,i));
                    rxk_next = MIP.opt_vars.pos(MIP.index.pos(1,k+1,i));
                    ryk = MIP.opt_vars.pos(MIP.index.pos(2,k,i));
                    ryk_next = MIP.opt_vars.pos(MIP.index.pos(2,k+1,i));
                    acc = MIP.opt_vars.acc(MIP.index.lin_vel(k,i)); % using index from linvel (equal)
                    linvel_k = MIP.opt_vars.lin_vel(MIP.index.lin_vel(k,i));
                    linvel_next = MIP.opt_vars.lin_vel(MIP.index.lin_vel(k+1,i));
                    
                    % Velocity model 
                    MIP.constraints.dyn = [MIP.constraints.dyn,
                        linvel_next <= linvel_k + acc*MIP.Ts + sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM,
                        -linvel_next <= -(linvel_k + acc*MIP.Ts) + sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM];
                    
                    % Position model 
                    for n = 1 : MIP.number_angles
                        B_orient = MIP.opt_vars.B_orient(MIP.index.B_orient(k,i,n));
                        
                        MIP.constraints.dyn = [MIP.constraints.dyn,
                            rxk_next <= rxk+(linvel_k*MIP.Ts+acc*(MIP.Ts^2/2))*round(cos(angles(n)),4)+(1-B_orient)*MIP.BigM+sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM,
                            ryk_next <= ryk+(linvel_k*MIP.Ts+acc*(MIP.Ts^2/2))*round(sin(angles(n)),4)+(1-B_orient)*MIP.BigM+sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM];
                        
                        MIP.constraints.dyn = [MIP.constraints.dyn,
                            -rxk_next <= -rxk-(linvel_k*MIP.Ts+acc*(MIP.Ts^2/2))*round(cos(angles(n)),4)+(1-B_orient)*MIP.BigM+sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM,
                            -ryk_next <= -ryk-(linvel_k*MIP.Ts+acc*(MIP.Ts^2/2))*round(sin(angles(n)),4)+(1-B_orient)*MIP.BigM+sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM];
                    end
                end
            end
            
            % This constraints imposes that the robot must be oriented
            % towards some of the predefined orientations 
            MIP.constraints.orient_binary = [];
            for i = 1 : MIP.number_robots
                for k = 1 : MIP.N + 1
                    MIP.constraints.orient_binary = [MIP.constraints.orient_binary,
                        sum(MIP.opt_vars.B_orient(MIP.index.B_orient(k,i,:))) == 1];
                end
            end
            
        end
        
        % Const. (33b), (33c), and partially (33d)
        function BuildPositionAndInputConstraints(MIP,scenario)
            
            poly = scenario.polytopes;
            
            % Position bounds
            rx_max = poly.op_region.q(1);
            ry_max = poly.op_region.q(2);
            rx_min = -poly.op_region.q(3);
            ry_min = -poly.op_region.q(4);
            
            MIP.constraints.acceleration = [];
            MIP.constraints.velocity = [];
            MIP.constraints.position = [];
            
            
            for k = 1 : MIP.N
                for i = 1 : MIP.number_robots
                    
                    % Var assignments
                    acc = MIP.opt_vars.acc(MIP.index.lin_vel(k,i));
                    lin_vel = MIP.opt_vars.lin_vel(MIP.index.lin_vel(k+1,i));
                    pos = MIP.opt_vars.pos(MIP.index.pos(:,k+1,i));
                    
                    % Acceleration constraint
                    MIP.constraints.acceleration = [MIP.constraints.acceleration,
                        acc <= scenario.agents{i}.max_acc + sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM,
                        acc >= -scenario.agents{i}.max_acc - sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM
                        ];
                    
                    % Velocity constraints
                    MIP.constraints.velocity = [MIP.constraints.velocity,
                        lin_vel <= scenario.agents{i}.max_vel + sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM,
                                                lin_vel >= 0 - sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM];
                    
                    % Position constraints
                      MIP.constraints.position = [MIP.constraints.position,
                        pos <= [rx_max;ry_max] + ones(2,1)*sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM,
                        -pos <= -[rx_min;ry_min] + ones(2,1)*sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM];
                end
            end
        end    
        
        % Partially const. (33d)
        function BuildCollisionAvoidanceConstraints(MIP,poly)
            
            MIP.constraints.collision_avoidance = [];
            MIP.constraints.collision_binary = [];
            MIP.constraints.collision_corner_cutting = [];
            for k = 1 : MIP.N + 1
                for i = 1 : MIP.number_robots-1
                    
                     pos_i = MIP.opt_vars.pos(MIP.index.pos(:,k,i));
                    
                    for j = i + 1 : MIP.number_robots
                        B_col = MIP.opt_vars.B_col(MIP.index.B_col(:,k,i,j));
                        
                        pos_j = MIP.opt_vars.pos(MIP.index.pos(:,k,j));
                            
                        
                        % Relative position
                        pos_diff = pos_i - pos_j;
                        
                        MIP.constraints.collision_avoidance = [MIP.constraints.collision_avoidance,
                            -poly.agent_body{i}.P*pos_diff<=-2*poly.agent_body{i}.q+(ones(4,1)-B_col)*MIP.BigM+ones(4,1)*sum(MIP.opt_vars.B_hor(1:k-1))*MIP.BigM];
                        
                        
                        if k <= MIP.N
                           
                              pos_i_next = MIP.opt_vars.pos(MIP.index.pos(:,k+1,i));
                              pos_j_next = MIP.opt_vars.pos(MIP.index.pos(:,k+1,j));
                            
                            % Next relative position
                            pos_diff_next = pos_i_next - pos_j_next;
                            
                            MIP.constraints.collision_corner_cutting = [MIP.constraints.collision_corner_cutting,
                                -poly.agent_body{i}.P*pos_diff_next<=-2*poly.agent_body{i}.q+(ones(4,1)-B_col)*MIP.BigM+ones(4,1)*sum(MIP.opt_vars.B_hor(1:k-1))*MIP.BigM];
                        end
                        
                        % obs binary constraint
                        MIP.constraints.collision_binary = [MIP.constraints.collision_binary,sum(B_col) >= 1];
                        
                        
                    end
                end
            end
            
        end
        
        % Const. (24)
        function BuildFieldOfViewConstraint(MIP,poly)
            
            % fov base constraints
            MIP.constraints.fov_base = [];
            for k = 1 : MIP.N + 1
                for i = 1 : MIP.number_robots
                    
                    % Pos i-th robot at time step k
                    pos_i = MIP.opt_vars.pos(MIP.index.pos(:,k,i));
                    
                    % Relative position of agent i w.r.t. base
                    rel_pos_base_i = pos_i - MIP.base.pos;
                    
                    % Base connectivity binary
                    B_con_base = MIP.opt_vars.B_con_base(MIP.index.base_con(k,i));
                    
                    % Cone FOV constraint for base
                    MIP.constraints.fov_base = [MIP.constraints.fov_base,
                        poly.fov_cones{MIP.base.orient_index}.P*rel_pos_base_i<=poly.fov_cones{MIP.base.orient_index}.q+ones(6,1)*(1-B_con_base)*MIP.BigM];
                end
            end
            
            
            % Inter-agent FOV constraints 
            MIP.constraints.fov = [];
            for k = 1 : MIP.N + 1
                for i = 1 : MIP.number_robots-1
                    
                    % Pos i-th robot at time step k
                    pos_i = MIP.opt_vars.pos(MIP.index.pos(:,k,i));
                    
                    if i~= MIP.number_robots % last robot doesnt have an emitter
                        
                        for j = 1 : MIP.number_robots
                            
                            if j~=i
                                B_con = MIP.opt_vars.B_con(MIP.index.B_con(k,i,j));
                                
                                % Pos j-th robot at time step k
                                pos_j = MIP.opt_vars.pos(MIP.index.pos(:,k,j));
                                
                                % Relative position (must always be j-th minus i-th since the orientation of the
                                % i-th robot is considered below
                                rel_pos_ij = pos_j - pos_i; % receiver - emitter
                                
                                for n = 1 : MIP.number_angles
                                    B_orient = MIP.opt_vars.B_orient(MIP.index.B_orient(k,i,n));
                                    
                                    MIP.constraints.fov = [MIP.constraints.fov,
                                        round(poly.fov_cones{n}.P,4)*rel_pos_ij<=poly.fov_cones{n}.q+ones(6,1)*(1-B_orient)*MIP.BigM+ones(6,1)*(1-B_con)*MIP.BigM];
                                    
                                end
                                
                            end
                        end
                    end
                end
            end
            
        end
        
        % Const. (33e) and Const. (33f)
        function BuildTargetConstraints(MIP,poly)
            
            MIP.constraints.targets = [];
            MIP.constraints.target_hor_eq  = [];
            MIP.constraints.tar_limit = [];
            MIP.constraints.no_reward_after_horizon = [];
            
            % Target entry
            for k = 1 : MIP.N + 1
                    pos = MIP.opt_vars.pos(MIP.index.pos(:,k,MIP.number_robots));
                    
                    for t = 1 : MIP.number_tar
                        B_tar = MIP.opt_vars.B_tar(MIP.index.B_tar(k,t));
                        MIP.constraints.targets = [MIP.constraints.targets,
                            poly.tar{t}.P*pos <= poly.tar{t}.q + ones(length(poly.tar{t}.q),1)*(1-B_tar)*MIP.BigM];
                    end
            end
            
            % Target binary conditions
            for k = 1 : MIP.N+1
                % Horizon is equal to sum of last target entry binary
                MIP.constraints.target_hor_eq = [MIP.constraints.target_hor_eq,
                     (sum(MIP.opt_vars.B_tar(MIP.index.B_tar(1:k,1))) + sum(MIP.opt_vars.B_tar(MIP.index.B_tar(1:k,2))))/2 >= MIP.opt_vars.B_hor(k)];
            end
            
            % Limit target collection
            for t = 1 : MIP.number_tar
                aux = [];
                for k = 1 : MIP.N + 1
                    aux = [aux;sum(MIP.opt_vars.B_tar(MIP.index.B_tar(k,t)))];
                end
                MIP.constraints.tar_limit = [MIP.constraints.tar_limit, sum(aux)<=1];
            end
            
            % Avoid collection after horizon
            for t = 1 : MIP.number_tar      
                    MIP.constraints.no_reward_after_horizon = [MIP.constraints.no_reward_after_horizon,
                        [1:MIP.N+1]*(MIP.opt_vars.B_tar(MIP.index.B_tar(:,t)) - MIP.opt_vars.B_hor) <= 0];
            end
            
            MIP.constraints.horizon = [sum(MIP.opt_vars.B_hor) == 1];
            
        end
        
        % Partially const. (33d)
        function BuildObstacleAvoidanceConstraints(MIP,poly)
            
            MIP.constraints.obstacles = [];
            MIP.constraints.obstacles_binary = [];
            MIP.constraints.obstacles_corner_cutting = [];
            for i = 1 : MIP.number_robots
                for k = 1 : MIP.N+1
                    
                    pos = MIP.opt_vars.pos(MIP.index.pos(:,k,i));
                        
                    if k <= MIP.N
                         pos_next = MIP.opt_vars.pos(MIP.index.pos(:,k+1,i));
                    end
                    
                    for j = 1 : MIP.number_obs
                        B_obs = MIP.opt_vars.B_obs(MIP.index.B_obs(:,k,i,j));
                        
                        % Obs entry constraint
                        MIP.constraints.obstacles = [MIP.constraints.obstacles,
                            -poly.obs{j}.P*pos <= -poly.obs{j}.q+(ones(4,1)-B_obs)*MIP.BigM+ones(4,1)*sum(MIP.opt_vars.B_hor(1:k-1))*MIP.BigM];
                        
                        % Corner cutting
                        if k <= MIP.N
                            MIP.constraints.obstacles_corner_cutting = [MIP.constraints.obstacles_corner_cutting,
                                -poly.obs{j}.P*pos_next <= -poly.obs{j}.q+(ones(4,1)-B_obs)*MIP.BigM+ones(4,1)*sum(MIP.opt_vars.B_hor(1:k))*MIP.BigM];
                        end
                        
                        % obs binary constraint
                        MIP.constraints.obstacles_binary = [MIP.constraints.obstacles_binary,
                            sum(B_obs) >= 1];
                    end
                end
            end
            
            
        end
        
        % Const. (15) to (20)
        function BuildConnectivityConstraints(MIP)
            
            B_con = MIP.opt_vars.B_con;
            % Builds in-degrees and out-degrees of each vertex at each time step
            for k = 1 : MIP.N + 1
                for i = 1 : MIP.number_robots
                    aux_out_degree = [];
                    aux_in_degree = MIP.opt_vars.B_con_base(MIP.index.base_con(k,i)); % contribution from base to in-degree of ith agent
       
                    for j = 1 : MIP.number_robots
                        if j ~= i
                            aux_out_degree = [aux_out_degree, B_con(MIP.index.B_con(k,i,j))];
                            aux_in_degree = [aux_in_degree, B_con(MIP.index.B_con(k,j,i))];
                        end
                    end
                    out_degree(i,k) = sum(aux_out_degree);
                    in_degree(i,k) = sum(aux_in_degree);
                end
            end

            % connectivity for digraphs
            MIP.constraints.di_conn  = [];
            for k = 1 : MIP.N + 1
                
                % base must be connected to someone
                MIP.constraints.di_conn = [MIP.constraints.di_conn,
                    sum(MIP.opt_vars.B_con_base(MIP.index.base_con(k,:))) ==1];
                % Notice that we dont need to explicitly enforce the
                % indegree of the base to be 0, since a connection of any
                % robot towards the base does not contribute to their
                % outdegree. Only connections among robots are considered
                % in the outdegree calculation.
                % leader 
                
                MIP.constraints.di_conn = [MIP.constraints.di_conn,
                    out_degree(MIP.number_robots,k) == 0,
                    in_degree(MIP.number_robots,k) == 1
                    ];
                
                % relays
                for i = 1 : MIP.number_robots-1
                    MIP.constraints.di_conn = [MIP.constraints.di_conn,
                        out_degree(i,k) == 1, % maybe <= to relax if not used
                        in_degree(i,k) == 1
                        ];
                end
            end
        end
        
        % Const. (22)
        % See
        %
        % "line of sight constraint based on intermediary points for 
        % "connectivity maintenance of multiagent systems using 
        % "mixed-Integer programming" (2022) European journal of control.
        %
        %  for details
        function BuildIntermediaryPointsLOSConstraint(MIP,poly)
            
            MIP.constraints.old_los = [];
            MIP.constraints.bin_old_los = [];
            MIP.constraints.los_corner_cut = [];
            MIP.constraints.old_los_base = [];
            MIP.constraints.bin_old_los_base  = [];
            MIP.constraints.los_corner_cut_base = [];
            
            B_con = MIP.opt_vars.B_con;
            B_obs = MIP.opt_vars.B_obs;
            B_hor = MIP.opt_vars.B_hor;
            
            % base
            for i = 1 : MIP.number_robots
                for k = 1 : MIP.N + 1
                    
                    pos_i = MIP.opt_vars.pos(MIP.index.pos(:,k,i));
                    for n = 1 : MIP.number_obs
                        for z = 1 : length(MIP.beta)-1
                            sample = MIP.base.pos*(1-MIP.beta(z)) + MIP.beta(z)*pos_i;
                            
                            MIP.constraints.old_los_base = [MIP.constraints.old_los_base,
                                -poly.obs{n}.P*sample <= -poly.obs{n}.q+MIP.BigM*(ones(4,1)-MIP.opt_vars.B_old_los_base(MIP.index.B_old_los_base(:,z,n,k,i)))];
                            
                            MIP.constraints.bin_old_los_base = [MIP.constraints.bin_old_los_base,...
                                MIP.opt_vars.B_con_base(MIP.index.base_con(k,i)) <= sum(MIP.opt_vars.B_old_los_base(MIP.index.B_old_los_base(:,z,n,k,i)))];
                            
                            % Now me must implement the corner cutting
                            % constraint. For that, the binary of the
                            % z-th sample must be a solution for the
                            % obstacle avoidance problem of the next
                            % sample
                            nextSample = MIP.base.pos*(1-MIP.beta(z+1)) + pos_i*MIP.beta(z+1);
                            
                            %
                            MIP.constraints.los_corner_cut_base = [MIP.constraints.los_corner_cut_base,...
                                -poly.obs{n}.P*nextSample <= -poly.obs{n}.q+MIP.BigM*(ones(4,1)-MIP.opt_vars.B_old_los_base(MIP.index.B_old_los_base(:,z,n,k,i)))+MIP.BigM*ones(4,1)*sum(B_hor(1:k-1))];
                        end % sample loop
                        
                        
                        % First and last samples with initial
                        % and last position
                        firstSample = MIP.base.pos*(1-MIP.beta(1)) + MIP.beta(1)*pos_i;
                        lastSample = MIP.base.pos*(1-MIP.beta(end)) + MIP.beta(end)*pos_i;
                        
                        
                        MIP.constraints.los_corner_cut_base = [MIP.constraints.los_corner_cut_base,...
                            -poly.obs{n}.P*firstSample <= -poly.obs{n}.q+MIP.BigM*(ones(4,1)-MIP.B_obs_base{n})+MIP.BigM*ones(4,1)*sum(B_hor(1:k-1))+ones(4,1)*(1-MIP.opt_vars.B_con_base(MIP.index.base_con(k,i)))*MIP.BigM];
                        
                        MIP.constraints.los_corner_cut_base = [MIP.constraints.los_corner_cut_base,...
                            -poly.obs{n}.P*lastSample <= -poly.obs{n}.q+MIP.BigM*(ones(4,1)-B_obs(MIP.index.B_obs(:,k,i,n)))+MIP.BigM*ones(4,1)*sum(B_hor(1:k-1))+ones(4,1)*(1-MIP.opt_vars.B_con_base(MIP.index.base_con(k,i)))*MIP.BigM];
                        
                        
                        
                    end
                end
            end
            
            % robots
            for j = 1 : MIP.number_robots-1
                for i = j + 1 : MIP.number_robots
                    for k = 1 : MIP.N + 1
                        
                        % Gets position Variable at k-th time step for
                        % a pair of agent
                        pos_i = MIP.opt_vars.pos(MIP.index.pos(:,k,i));
                        pos_j = MIP.opt_vars.pos(MIP.index.pos(:,k,j));
                        
                        for n = 1 : MIP.number_obs
                            
                            for z = 1 : length(MIP.beta)-1
                                
                                % build LOS samples
                                sample = pos_j*(1-MIP.beta(z)) + MIP.beta(z)*pos_i;
                                
                                % Collision avoidance constraint for the % z-th sample
                                MIP.constraints.old_los = [MIP.constraints.old_los,
                                    -poly.obs{n}.P*sample <= -poly.obs{n}.q+MIP.BigM*(ones(4,1)-MIP.opt_vars.B_old_los(MIP.index.B_old_los(:,z,n,k,j,i)))];
                                
                                MIP.constraints.bin_old_los = [MIP.constraints.bin_old_los,...
                                    B_con(MIP.index.B_con(k,i,j)) <= sum(MIP.opt_vars.B_old_los(MIP.index.B_old_los(:,z,n,k,j,i))),
                                    B_con(MIP.index.B_con(k,j,i)) <= sum(MIP.opt_vars.B_old_los(MIP.index.B_old_los(:,z,n,k,j,i)))];
                                
                                
                                % Now me must implement the corner cutting
                                % constraint. For that, the binary of the
                                % z-th sample must be a solution for the
                                % obstacle avoidance problem of the next
                                % sample
                                nextSample = pos_j*(1-MIP.beta(z+1)) + pos_i*MIP.beta(z+1);
                                
                                %
                                MIP.constraints.los_corner_cut = [MIP.constraints.los_corner_cut,...
                                    -poly.obs{n}.P*nextSample <= -poly.obs{n}.q+MIP.BigM*(ones(4,1)-MIP.opt_vars.B_old_los(MIP.index.B_old_los(:,z,n,k,j,i)))+MIP.BigM*ones(4,1)*sum(B_hor(1:k-1))];
                            end % sample loop
                            
                            
                            % The corner cutting inside the sample loop is
                            % between samples. Here I apply the constraint
                            % to the pairs (y_1,sample(1)) and
                            % (sample(end),y_2)
                            firstSample = pos_j*(1-MIP.beta(1)) + MIP.beta(1)*pos_i;
                            lastSample = pos_j*(1-MIP.beta(end)) + MIP.beta(end)*pos_i;
                            
                            %MIP.B_obs(MIP.B_obsIndex(:,n,k,j))
                            con_bin_sum = B_con(MIP.index.B_con(k,i,j)) + B_con(MIP.index.B_con(k,j,i));
                            MIP.constraints.los_corner_cut = [MIP.constraints.los_corner_cut,...
                                -poly.obs{n}.P*firstSample <= -poly.obs{n}.q+MIP.BigM*(ones(4,1)-B_obs(MIP.index.B_obs(:,k,j,n)))+MIP.BigM*ones(4,1)*sum(B_hor(1:k-1))+ones(4,1)*(1-con_bin_sum)*MIP.BigM];
                            MIP.constraints.los_corner_cut = [MIP.constraints.los_corner_cut,...
                                -poly.obs{n}.P*lastSample <= -poly.obs{n}.q+MIP.BigM*(ones(4,1)-B_obs(MIP.index.B_obs(:,k,i,n)))+MIP.BigM*ones(4,1)*sum(B_hor(1:k-1))+ones(4,1)*(1-con_bin_sum)*MIP.BigM];
                            
                        end %  obstacle loop
                    end %  time loop
                end  % agent j loop
            end % agent i loop
            
            
            
        end
        
        % Const. (30)
        function BuildOrientationIncrementConstraint(MIP)
            
            MIP.constraints.orient_increment = [];
            for i = 1 : MIP.number_robots
                for k = 2 : MIP.N+1
                    MIP.constraints.orient_increment = [MIP.constraints.orient_increment,
                    MIP.opt_vars.abs_orient_diff(MIP.index.abs_orient_diff(k-1,i)) <= pi/3];
                end
            end

        end
        
        % For the cost
        function BuildAccelerationAbsoluteValueConstraint(MIP)
           MIP.constraints.abs_acc = [-MIP.opt_vars.abs_acc <= MIP.opt_vars.acc <= MIP.opt_vars.abs_acc]; 
        end
        
        % Const. (28), (29), and (30) and partially cost
        function BuildOrientationPenalization(MIP)
            
            MIP.constraints.abs_orient_diff = [];
            MIP.constraints.sense = [];
            
           % angles = [0:2*pi/MIP.number_angles:2*pi];
           angles = [-pi:2*pi/MIP.number_angles:pi];
            angles = angles(1:end-1); % ignore last entry as 0 = 2pi
            
            for i = 1 : MIP.number_robots
                for k = 2 : MIP.N+1
                    B_orient_pen_1 = MIP.opt_vars.B_orient_pen_1(MIP.index.abs_orient_diff(k-1,i));
                    B_orient_pen_2 = MIP.opt_vars.B_orient_pen_2(MIP.index.abs_orient_diff(k-1,i));
                    orient_diff =  angles*(MIP.opt_vars.B_orient(MIP.index.B_orient(k,i,:))-MIP.opt_vars.B_orient(MIP.index.B_orient(k-1,i,:))); 

                     MIP.constraints.sense = [MIP.constraints.sense,
                        orient_diff <= pi + B_orient_pen_1*MIP.BigM;
                        -orient_diff <= pi +  B_orient_pen_2*MIP.BigM];
                    
                    MIP.constraints.abs_orient_diff = [MIP.constraints.abs_orient_diff,
                        -MIP.opt_vars.abs_orient_diff(MIP.index.abs_orient_diff(k-1,i)) <=  orient_diff - 2*pi*B_orient_pen_1 + 2*pi*B_orient_pen_2 <= MIP.opt_vars.abs_orient_diff(MIP.index.abs_orient_diff(k-1,i))];
                end
            end
        end
        
        % Const. (25) and (26)
        function BuildNoFrontConnectionConstraint(MIP,poly)
            
            % BASE
            MIP.constraints.no_front_conn_base = [];
            MIP.constraints.no_front_conn_base_bin = [];
            for k = 1 : MIP.N + 1
                for i = 1 : MIP.number_robots-1
                    
%                     % Pos i-th robot at time step k
                    pos_i = MIP.opt_vars.pos(MIP.index.pos(:,k,i));
                    
                    % Relative position of agent i w.r.t. base
                    rel_pos_base_i = -(pos_i - MIP.base.pos); % inverted
                    
                    B_con_base = MIP.opt_vars.B_con_base(MIP.index.base_con(k,i));
                    
                    for n = 1 : MIP.number_angles
                        B_front_base = MIP.opt_vars.B_front_base(MIP.index.B_front_base(:,n,k,i));
                        B_orient = MIP.opt_vars.B_orient(MIP.index.B_orient(k,i,n));
                        
                        MIP.constraints.no_front_conn_base = [MIP.constraints.no_front_conn_base,
                            -poly.fov_cones{n}.P*rel_pos_base_i<=-poly.fov_cones{n}.q+(ones(6,1)-B_front_base)*MIP.BigM];
                        
                        MIP.constraints.no_front_conn_base_bin = [MIP.constraints.no_front_conn_base_bin,
                        B_con_base <= sum(B_front_base) + (1-B_orient)];
                        
                    end
                    
                end
                
            end
            
            % RELAYS
            MIP.constraints.no_front_conn = [];
            MIP.constraints.no_front_conn_bin = [];
            for k = 1 : MIP.N + 1
                for i = 1 : MIP.number_robots-1
                    pos_i = MIP.opt_vars.pos(MIP.index.pos(:,k,i));
                    for j = 1 : MIP.number_robots-1
                        % Pos i-th robot at time step k
                        if i ~= j
                            
                            pos_j = MIP.opt_vars.pos(MIP.index.pos(:,k,j));
                            
                            % Relative position of agent i w.r.t. base
                            rel_pos_j_i = -(pos_j - pos_i); % inverted
                            
                            B_con = MIP.opt_vars.B_con(MIP.index.B_con(k,i,j));
                            
                            for n = 1 : MIP.number_angles
                                B_front = MIP.opt_vars.B_front(MIP.index.B_front(:,n,k,i,j));
                                B_orient = MIP.opt_vars.B_orient(MIP.index.B_orient(k,j,n));
                                
                                MIP.constraints.no_front_conn = [MIP.constraints.no_front_conn,
                                    -poly.fov_cones{n}.P*rel_pos_j_i<=-poly.fov_cones{n}.q+(ones(6,1)-B_front)*MIP.BigM];
                                
                                MIP.constraints.no_front_conn_bin = [MIP.constraints.no_front_conn_bin,
                                    B_con <= sum(B_front) + (1-B_orient)];
                                
                            end
                        end
                    end
                    
                end
                
            end

%                    % Pos i-th robot at time step k
%                     pos_i = MIP.opt_vars.pos(MIP.index.pos(:,k,i));
%                     
%                     % Relative position of agent i w.r.t. base
%                     rel_pos_base_i = -(pos_i - MIP.base.pos); % inverted
%                     
%                     B_con_base = MIP.opt_vars.B_con_base(MIP.index.base_con(k,i));
%                     
%                     for n = 1 : MIP.number_angles
%                         B_front_base = MIP.opt_vars.B_front_base(MIP.index.B_front_base(:,n,k,i));
%                         B_orient = MIP.opt_vars.B_orient(MIP.index.B_orient(k,i,n));
%                         
%                         MIP.constraints.no_front_conn_base = [MIP.constraints.no_front_conn_base,
%                             -poly.fov_cones{n}.P(end-1:end,:)*rel_pos_base_i<=-poly.fov_cones{n}.q(end-1:end)+(ones(2,1)-B_front_base)*MIP.BigM];
%                         
%                         MIP.constraints.no_front_conn_base_bin = [MIP.constraints.no_front_conn_base_bin,
%                         B_con_base <= sum(B_front_base) + (1-B_orient)];
%                         
%                     end
%                     
%                 end
%                 
%             end
%             
%             % RELAYS
%             MIP.constraints.no_front_conn = [];
%             MIP.constraints.no_front_conn_bin = [];
%             for k = 1 : MIP.N + 1
%                 for i = 1 : MIP.number_robots-1
%                     pos_i = MIP.opt_vars.pos(MIP.index.pos(:,k,i));
%                     for j = 1 : MIP.number_robots-1
%                         % Pos i-th robot at time step k
%                         if i ~= j
%                             
%                             pos_j = MIP.opt_vars.pos(MIP.index.pos(:,k,j));
%                             
%                             % Relative position of agent i w.r.t. base
%                             rel_pos_j_i = -(pos_j - pos_i); % inverted
%                             
%                             B_con = MIP.opt_vars.B_con(MIP.index.B_con(k,i,j));
%                             
%                             for n = 1 : MIP.number_angles
%                                 B_front = MIP.opt_vars.B_front(MIP.index.B_front(:,n,k,i,j));
%                                 B_orient = MIP.opt_vars.B_orient(MIP.index.B_orient(k,j,n));
%                                 
%                                 MIP.constraints.no_front_conn = [MIP.constraints.no_front_conn,
%                                     -poly.fov_cones{n}.P(end-1:end,:)*rel_pos_j_i<=-poly.fov_cones{n}.q(end-1:end)+(ones(2,1)-B_front)*MIP.BigM];
%                                 
%                                 MIP.constraints.no_front_conn_bin = [MIP.constraints.no_front_conn_bin,
%                                     B_con <= sum(B_front) + (1-B_orient)];
%                                 
%                             end
%                         end
%                     end
%                     
%                 end
%                 
%             end
            
            
            
        end
       
        % Cost
        function BuildCost(MIP)
            
            MIP.cost = [1:MIP.N+1]*MIP.opt_vars.B_hor + MIP.gamma_a*sum(MIP.opt_vars.abs_acc) + MIP.gamma_incr*sum(MIP.opt_vars.abs_orient_diff);
           
        end
        
    end % end methods
end % end class

