function scenario_struct = TB3_BigField_1()

%% Agents TURTLEBOT 3 BURGUER

% General margin for robustness
safety_margin = 0.08;

% Build cone and orientation polytopes
number_angles = 360/30; % divide in angles 15 degrees apart
angle = [0:2*pi/number_angles:2*pi];

% cone robustness margin
cone_margin = 0.22; %from maximum displacement of tb3 in 1s]

% Original light cone parameters
cone_range = 2.8;
cone_angle = pi/3;


for i = 1 : number_angles
    
    
    rot_M = [cos(angle(i)), -sin(angle(i)); sin(angle(i)), cos(angle(i))];
    
    v_cone_orig{i}(1,:) = [0;0];
    v_cone_orig{i}(2,:) = rot_M*[cone_range*cos(cone_angle/2) ; cone_range*sin(cone_angle/2)];
    v_cone_orig{i}(3,:) = rot_M*[cone_range*cos(cone_angle/4) ; cone_range*sin(cone_angle/4)];
    v_cone_orig{i}(4,:) = rot_M*[cone_range;0];
    v_cone_orig{i}(5,:) = rot_M*[cone_range*cos(cone_angle/2) ; -cone_range*sin(cone_angle/2)];
    v_cone_orig{i}(6,:) = rot_M*[cone_range*cos(cone_angle/4) ; -cone_range*sin(cone_angle/4)];
    
    fov_cone_orig_vrep = Polyhedron(v_cone_orig{i});
    fov_cone_orig_hrep = fov_cone_orig_vrep.computeHRep;
    scenario_struct.polytopes.fov_cones_orig{i}.P = fov_cone_orig_hrep.A;
    scenario_struct.polytopes.fov_cones_orig{i}.q = fov_cone_orig_hrep.b;
    
    % new
    % Shrinked cone
    q = zeros(length(scenario_struct.polytopes.fov_cones_orig{i}.q),1);
    for v = 1 : length(scenario_struct.polytopes.fov_cones_orig{i}.q)
        d = (cone_margin+safety_margin);
        h = scenario_struct.polytopes.fov_cones_orig{i}.P(v,:);
        % this operation is analogous to a pontryagin difference between the cone and a ball
        q(v) = scenario_struct.polytopes.fov_cones_orig{i}.q(v) - d*norm(h) ;
    end
    scenario_struct.polytopes.fov_cones{i}.P = scenario_struct.polytopes.fov_cones_orig{i}.P;
    scenario_struct.polytopes.fov_cones{i}.q = q;
    
end



% TB3 body is considered as a square with side equal to the maximum
% diameter of the circle that it produces when rotating
tb3_width = 0.138;
tb3_length = 0.178;
radius_circle = sqrt(tb3_width^2 + tb3_length^2)/2; % radius of the circle that contains the agent body
body_size = radius_circle*2; %square that contains above circle

% Base parameters
scenario_struct.base.pos = [-1.5;1.25];
scenario_struct.base.orient_index = 10;

ini_state =  [ -1.5 -1.5 -1.5;
    0.0 0.0 0.0;
    0.6 -0.05 -0.75;
    0.0 0.0 0.0];



% Number of agents
scenario_struct.number_agents = size(ini_state,2);

for i = 1 : scenario_struct.number_agents
    
    scenario_struct.agents{i}.x0 = ini_state(:,i);
    scenario_struct.agents{i}.max_vel = 0.22;
    scenario_struct.agents{i}.max_acc = 0.22;
    
    % Body polytope
    scenario_struct.polytopes.agent_body{i}.P = [eye(2);-eye(2)];
    scenario_struct.polytopes.agent_body{i}.q = [body_size + safety_margin/2;
        body_size + safety_margin/2 ;
        body_size + safety_margin/2;
        body_size + safety_margin/2];
    
    
    scenario_struct.polytopes.agent_body_orig{i}.P = [eye(2);-eye(2)];
    scenario_struct.polytopes.agent_body_orig{i}.q = [tb3_width/2 ;
        tb3_length/2 ;
        tb3_width/2 ;
        tb3_length/2 ];
end


%% Operation region

op_region_width = 4;
op_region_length = 3;
op_region_center = [0,0];

% Point mass region
scenario_struct.polytopes.op_region.P = [eye(2);-eye(2)];
scenario_struct.polytopes.op_region.q = [op_region_center(1) + (op_region_width - safety_margin)/2;
    op_region_center(2) + (op_region_length - safety_margin)/2 ;
    op_region_center(1) + (op_region_width - safety_margin)/2;
    op_region_center(2) + (op_region_length - safety_margin)/2];

scenario_struct.polytopes.op_region.P = [eye(2);-eye(2)];
scenario_struct.polytopes.op_region.q = [op_region_center(1) + (op_region_width - safety_margin)/2;
    op_region_center(2) + (op_region_length - safety_margin)/2;
    op_region_center(1) + (op_region_width - safety_margin)/2;
    op_region_center(2) + (op_region_length - safety_margin)/2];

% Original region
scenario_struct.polytopes.op_region_orig.P = [eye(2);-eye(2)];
scenario_struct.polytopes.op_region_orig.q = [op_region_center(1) + (body_size+op_region_width)/2;
    op_region_center(2) + (body_size+op_region_length)/2;
    op_region_center(1) + (body_size+op_region_width)/2;
    op_region_center(2) + (body_size+op_region_length)/2];

scenario_struct.polytopes.op_region_orig.P = [eye(2);-eye(2)];
scenario_struct.polytopes.op_region_orig.q = [op_region_center(1) + (body_size+op_region_width)/2;
    (body_size+op_region_center(2) + op_region_length)/2;
    (body_size+op_region_center(1) + op_region_width)/2;
    (body_size+op_region_center(2) + op_region_length)/2];


%% Obstacles

% Obstacle 1
obs_original_dimensions{1} = [0.0520,1.4930];
obs_center{1} = [-0.75,0.8655];
obs_dimensions{1} = [obs_original_dimensions{1}(1) + body_size + safety_margin,
    obs_original_dimensions{1}(2) + body_size + safety_margin];

% Obstacle 2
obs_original_dimensions{2} = [0.0520,1.4930];
obs_center{2} = [1,-0.8655];
obs_dimensions{2} = [obs_original_dimensions{2}(1) + body_size + safety_margin,
    obs_original_dimensions{2}(2) + body_size + safety_margin];

scenario_struct.number_obs = length(obs_center);

for i = 1 : scenario_struct.number_obs
    
    % Original
    scenario_struct.polytopes.obs{i}.P = [eye(2);-eye(2)];
    scenario_struct.polytopes.obs{i}.q = [obs_center{i}(1) + obs_dimensions{i}(1)/2;
        obs_center{i}(2) + obs_dimensions{i}(2)/2;
        -(obs_center{i}(1) - obs_dimensions{i}(1)/2);
        -(obs_center{i}(2) - obs_dimensions{i}(2)/2)];
    
    % Expanded (robot size + safety margin)
    scenario_struct.polytopes.obs_orig{i}.P = [eye(2);-eye(2)];
    scenario_struct.polytopes.obs_orig{i}.q = [obs_center{i}(1) + obs_original_dimensions{i}(1)/2;
        obs_center{i}(2) + obs_original_dimensions{i}(2)/2;
        -(obs_center{i}(1) - obs_original_dimensions{i}(1)/2);
        -(obs_center{i}(2) - obs_original_dimensions{i}(2)/2)];
    
end



%% Mission

% Targets (Shrinked)
tar_center{1} = [1.5,-0.5];
tar_dimensions{1} = [0.4,0.4]; % width x length

tar_center{2} = [0.25,0.75];
tar_dimensions{2} = [0.4,0.4]; % width x length

scenario_struct.number_tar = length(tar_center);

for i = 1 : scenario_struct.number_tar
    scenario_struct.polytopes.tar{i}.P = [eye(2);-eye(2)];
    scenario_struct.polytopes.tar{i}.q = [tar_center{i}(1) + (tar_dimensions{1}(1)-safety_margin)/2;
        tar_center{i}(2) + (tar_dimensions{i}(2)-safety_margin)/2;
        -(tar_center{i}(1) - (tar_dimensions{i}(1)-safety_margin)/2);
        -(tar_center{i}(2) - (tar_dimensions{i}(2)-safety_margin)/2)];
   
    
end


end

