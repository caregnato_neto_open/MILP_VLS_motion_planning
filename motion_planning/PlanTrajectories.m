
%% Load scenario
clc; clearvars; close all;
cd scenarios
scen_handle = TB3_BigField_1;
cd ../
scenario_name = "TB3_BigField_1";   
scenario = scenarioClass(scen_handle);

%% Build optimization model
MIP = MIPClass(scenario);
MIP.BuildOptVarIndexes();

timer = tic; MIP.BuildNonlinearDynamicsConstraints(scenario.agents);
disp("Dynamic const build time: "+toc(timer));

timer = tic;MIP.BuildPositionAndInputConstraints(scenario);
disp("Pos and input const build time: "+toc(timer));

timer = tic; MIP.BuildTargetConstraints(scenario.polytopes);
disp("Target const build time: "+toc(timer));

timer = tic; MIP.BuildObstacleAvoidanceConstraints(scenario.polytopes);
disp("Obs const build time: "+toc(timer));

timer = tic; MIP.BuildCollisionAvoidanceConstraints(scenario.polytopes);
disp("Col const build time: "+toc(timer));

timer = tic; MIP.BuildConnectivityConstraints();
disp("Conn const build time: "+toc(timer));

timer = tic; MIP.BuildIntermediaryPointsLOSConstraint(scenario.polytopes);
disp("LOS const build time: "+toc(timer));

timer = tic; MIP.BuildFieldOfViewConstraint(scenario.polytopes)
disp("FOV const build time: "+toc(timer));

timer = tic; MIP.BuildAccelerationAbsoluteValueConstraint()
disp("Acc. abs const build time: "+toc(timer));

timer = tic; MIP.BuildOrientationPenalization()
disp("Orientation penalization build time: "+toc(timer));

timer = tic; MIP.BuildOrientationIncrementConstraint()
disp("Orientation increment const build time: "+toc(timer));

timer = tic; MIP.BuildNoFrontConnectionConstraint(scenario.polytopes)
disp("No front const build time: "+toc(timer));

constraints = [
    MIP.constraints.dyn
    MIP.constraints.initial_condition
    MIP.constraints.position
    MIP.constraints.acceleration
    MIP.constraints.velocity
    MIP.constraints.abs_acc
    MIP.constraints.targets
    MIP.constraints.target_hor_eq
    MIP.constraints.horizon
    MIP.constraints.tar_limit
    MIP.constraints.no_reward_after_horizon
    MIP.constraints.obstacles
    MIP.constraints.obstacles_binary
    MIP.constraints.obstacles_corner_cutting
    MIP.constraints.collision_avoidance
    MIP.constraints.collision_binary
    MIP.constraints.collision_corner_cutting
    MIP.constraints.orient_binary
    MIP.constraints.di_conn
    MIP.constraints.old_los
    MIP.constraints.bin_old_los
    MIP.constraints.los_corner_cut
    MIP.constraints.old_los_base
    MIP.constraints.bin_old_los_base
    MIP.constraints.los_corner_cut_base
    MIP.constraints.fov
    MIP.constraints.fov_base
    MIP.constraints.abs_orient_diff
    MIP.constraints.orient_increment 
    MIP.constraints.sense
   MIP.constraints.no_front_conn_base 
   MIP.constraints.no_front_conn_base_bin
    MIP.constraints.no_front_conn 
    MIP.constraints.no_front_conn_bin
    ];

% Cost
timer = tic;
MIP.BuildCost();
tfinal = toc(timer);
disp("Cost build time: " + tfinal);

%% Solve
options = sdpsettings('solver','gurobi');
options.gurobi.Heuristics = 0.025; % only fine-tuning required

disp("Starting optimization");
timer = tic;
diagnostics = optimize(constraints,MIP.cost,options);
disp("Optimization time: "+toc(timer));

%% Analysis
close all;

data_processing = DataClass(MIP,scenario);
data = data_processing.BuildFileForStorage();
data_processing.BuildColors(data);
data_processing.PlotSnapshots(data);
data_processing.PlotLinearVelocities(data);
data_processing.PlotOrientations(data);
data_processing.PlotAccelerations(data);
data_processing.PlotCommunicationNetwork(data);

% Traj for gazebo
traj = data.traj;
traj.Ts = MIP.Ts;
save('./results/traj_gzb_' + scenario_name,'traj')


