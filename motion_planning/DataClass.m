classdef DataClass < handle
    % Display data class
    
    properties
        mip = [];
        scenario = [];
        N_opt = [];
        traj = [];
        colors = [];
        agents = [];
        pos = []
        lin_vel = []
        acc = []
        orientation = []
    end
    
    methods
        function obj = DataClass(mip,scenario)
            
            % Extract all variables from MIP object
            
            % Horizon
            obj.N_opt = find(value(mip.opt_vars.B_hor) == 1);
            
            % traj
            for i = 1 : mip.number_robots
                for k = 1 : obj.N_opt
                    obj.pos(:,k,i) = value(mip.opt_vars.pos(mip.index.pos(:,k,i)));
                    obj.lin_vel(k,i) = value(mip.opt_vars.lin_vel(mip.index.lin_vel(k,i)));
                    obj.acc(k,i) = value(mip.opt_vars.acc(mip.index.lin_vel(k,i)));
                    obj.orientation(k,i) = (2*pi/mip.number_angles)*(find(round(value(mip.opt_vars.B_orient(mip.index.B_orient(k,i,:),1)))==1)-1);
                end
            end
            
            
            
            obj.mip = mip;
            obj.scenario = scenario;
            obj.N_opt = find(value(mip.opt_vars.B_hor) == 1);
            obj.agents = scenario.agents;


        end
        
        %%%%%%%%%
        function PlotTrajectories(obj)
            
            obj.scenario.Plot(1);
            for i = 1 : data.number_robots
                plot(obj.pos(1,1:obj.N_opt,i),obj.pos(2,1:obj.N_opt,i));
            end
            xlabel('r_x'); ylabel('r_y');
            
        end
        
        %%%%%%%%%
        function BuildColors(obj,data)
            % Pre-allocation
            % Colors
            obj.colors = zeros(data.number_robots,3);
            
            % Assign pallet of colors to variable
            c = jet;
            
            % First plt.Data.agent is always assing to the color in the first index
            obj.colors(1,:) = c(1,:);
            
                        % Assign colors to the remaining
            for i = 2 : data.number_robots
                obj.colors(i,:) = c(i*floor(length(c)/data.number_robots),:);
            end
        end
        
        %%%%%%%%%
        function PlotScenario(obj,timestep,data)
            
                       % Colors
            data.agents{1}.color = 'blue'; % base
            data.agents{end}.color = 'red'; % leader
            for i = 2 : data.number_robots-1; data.agents{i}.color = 'green'; end % followers
            
            
            % Plot operational region
            figure(timestep); hold on;
            field = Polyhedron(data.polytopes.op_region_orig.P,data.polytopes.op_region_orig.q);
            field.plot('color','white','alpha',0.2)
            % plot(Polyhedron(obj.polytopes.op_region.P,obj.polytopes.op_region.q),'color','white','alpha',1);
            
            % Plot obstacles
            for i=1:data.number_obs
                plot(Polyhedron(data.polytopes.obs{i}.P,data.polytopes.obs{i}.q),'color','black');
            end
            
            % Plot targets
            for i=1:data.number_tar
                plot(Polyhedron(data.polytopes.tar{i}.P,data.polytopes.tar{i}.q),'color','red');
            end
            
            % Initial positions
            for i = 1 : data.number_robots
                q = [data.agents{i}.x0(1);
                    data.agents{i}.x0(3);
                    -data.agents{i}.x0(1);
                    -data.agents{i}.x0(3)]+...
                    [data.polytopes.agent_body{i}.q(1)/2;
                    data.polytopes.agent_body{i}.q(2)/2;
                    +data.polytopes.agent_body{i}.q(3)/2;
                    +data.polytopes.agent_body{i}.q(4)/2];
% %                 
%                                  plot(obj.agents{i}.x0(1),obj.agents{i}.x0(3),'o')
%                                   plot(Polyhedron(obj.polytopes.agent_body{i}.P,q),'color',obj.agents{i}.color);
            end
             
            
            
        end
        
        %%%%%%%%
        function PlotSnapshots(obj,data)
            
            

                       
            % Builds legends
            legendtest{1} = "Base";
            for i = 2 : data.number_robots
                legendtest{i} = "Relay "+(i-1);
            end
             legendtest{data.number_robots+1} = "Leader";
            
            
            
            figure(); hold on; grid;

            for k = 1 : data.N_opt
                obj.PlotScenario(k,data);

                
                % Plot base and cone
                basefig = plot(data.base.pos(1),data.base.pos(2),'s','color','b','MarkerSize',15,'MarkerFaceColor','b');
                cone = Polyhedron(data.polytopes.fov_cones{data.base.orient_index}.P,data.polytopes.fov_cones{data.base.orient_index}.q);
                cone_v = cone.computeVRep;
                cone_v_trans = Polyhedron(cone_v.V+[ones(6,1)*data.base.pos(1),ones(6,1)*data.base.pos(2)]);
                cone_v_trans.plot('color','yellow','alpha',0.2);
                
                
                relayfig = [];
                for i = 1 : data.number_robots
                                    for j = 1 : k
                    plot(data.traj.pos(1,j,i),data.traj.pos(2,j,i),'o','color',obj.colors(i,:),'MarkerFaceColor', obj.colors(i,:));
                end
                    plot(data.traj.pos(1,1:k,i),data.traj.pos(2,1:k,i),'LineWidth',2,'color',obj.colors(i,:));
                   
                    % Plot position
%                     switch i     
%                         case obj.mip.number_robots %leader
%                         leaderfig  = plot(obj.traj(1,k,i),obj.traj(2,k,i),'^','color',obj.colors(i,:),'MarkerSize',10,'MarkerFaceColor',obj.colors(i,:));        
%                         otherwise
                            translated_q = data.polytopes.agent_body{i}.q/2 + [data.traj.pos(1,k,i);data.traj.pos(2,k,i);-data.traj.pos(1,k,i);-data.traj.pos(2,k,i)];
                            body_poly = Polyhedron(data.polytopes.agent_body{i}.P,translated_q);
                       % relayfig = [relayfig, plot(obj.traj(1,k,i),obj.traj(2,k,i),'s','color',obj.colors(i,:),'MarkerSize',30,'MarkerFaceColor',obj.colors(i,:))];
                       relayfig = [relayfig,body_poly.plot('Color',obj.colors(i,:))];
                       
%                     end
                    % Plot cone
                    if i < data.number_robots
                   angle_index = find(round(value(data.B_orient(data.index.B_orient(k,i,:))),0)==1);
                   
                   if(isempty(angle_index))
                       display("empty angle index"); 
                   end
                   
                   cone = Polyhedron(data.polytopes.fov_cones{angle_index}.P,data.polytopes.fov_cones{angle_index}.q);
                   cone_v = cone.computeVRep;
                   cone_v_trans = Polyhedron(cone_v.V+[ones(6,1)*data.traj.pos(1,k,i),ones(6,1)*data.traj.pos(2,k,i)]);
                   cone_v_trans.plot('color','yellow','alpha',0.2);
                   end
                    
                end
               % legend([basefig  relayfig leaderfig],legendtest)
                legend([basefig  relayfig],legendtest)
             %   print("sim_col_"+k,'-painters','-dpdf')
            end
            
            
        end
        
        %%%%%%%%%
        function PlotCommunicationNetwork(obj,data)
            
            n_col = 3; n_row = 3;
            
            j = 0; figure;
            for k = 1 : data.N_opt
                names = [];
                aux = 0;
                names = ["Base"];
                for i=2:data.number_robots
                    names = [names,"Relay"+(i-1)];
                end
                
                Adj = zeros(data.number_robots);
                for i = 1 : data.number_robots
                    for j = 1: data.number_robots
                        if j~=i
                            Adj(i,j) = data.B_con(data.index.B_con(k,i,j));
                                
                            %Adj(j,i) = obj.mip.opt_vars.B_con(obj.mip.index.B_con(k,i,j));
                        end
                    end
                end
                
                Adj = round(Adj,1);
                
                
                % add base to adjacency matrix
                for i = 1 : data.number_robots
                    aux = [aux, value(data.B_con_base(data.index.base_con(k,i)))];
                    
                end
                Adj = [aux;
                    zeros(data.number_robots,1),Adj];
                
                names = [names,"Leader"];
                
                dig = digraph(Adj,names);
                subplot(n_row,n_col,k)
                plot(dig);
                title("k="+k);
                
                
            end
            
            
        end
        
        %%%%%%%%%
        function PlotOrientations(obj,data)
            max_number_subplot_columns = 3;
            column_coord = 0;
            row_coord = 1;

            for i = 1 : data.number_robots
                 column_coord = column_coord + 1;
                if column_coord >  max_number_subplot_columns
                    row_coord = row_coord + 1;
                
                 column_coord= 1;
                end
                plot_coord(:,i) = [row_coord;column_coord];
            end
            
            figure(); 
            for i = 1 : data.number_robots
                for k = 1 : data.N_opt
                    %orientation(k,i) = (pi/12)*(find(round(data.B_orient(data.index.B_orient(k,i,:),1))==1)-1);
                    subplot(size(plot_coord,1),min(data.number_robots,max_number_subplot_columns),i);
                    plot(k-1,obj.orientation(k,i)*180/pi,'o'); hold on; grid on; 
                    xlim([0 data.N_opt]); ylim([0 360]);
                    xticks([0:1:data.N_opt]);
                    yticks([0:60:360]);
                    xlabel('Time (s)'); ylabel('Orientation (degrees)');
                    title("Robot "+i)
                    
                end
                hold off;
                
            end
        end
        
        %%%%%%%%%
        function PlotLinearVelocities(obj,data)
            
            max_number_subplot_columns = 3;
            column_coord = 0;
            row_coord = 1;
            
            for i = 1 : data.number_robots
                column_coord = column_coord + 1;
                if column_coord >  max_number_subplot_columns
                    row_coord = row_coord + 1;
                    
                    column_coord= 1;
                end
                plot_coord(:,i) = [row_coord;column_coord];
            end
            
            figure(); 
            
            for i = 1 : data.number_robots
%                    for k = 1 : data.N_opt
%                        lin_vel(k,i) = norm(data.vel(:,k,i));
%                    end
                       
                
                    subplot(size(plot_coord,1),min(data.number_robots,max_number_subplot_columns),i);
                    plot([0:data.N_opt-1],value(data.traj.lin_vel(:,i)),'o'); grid;
                   % plot([0:data.N_opt-1],lin_vel(:,i)); grid;
                    xlim([0 data.N_opt]); ylim([0 1]);
                    xticks([0:1:data.N_opt]);
                    %yticks([0:60:360]);
                    xlabel('Time (s)'); ylabel('Linear velocity (m/s)');
                    title("Robot "+i)
            end
            
            
            
        end
        
        %%%%%%%%%
        function PlotAccelerations(obj,data)
            
            max_number_subplot_columns = 3;
            column_coord = 0;
            row_coord = 1;
            
            for i = 1 : data.number_robots
                column_coord = column_coord + 1;
                if column_coord >  max_number_subplot_columns
                    row_coord = row_coord + 1;
                    
                    column_coord= 1;
                end
                plot_coord(:,i) = [row_coord;column_coord];
            end
            
            figure(); 
            
            for i = 1 : data.number_robots
%                    for k = 1 : data.N_opt
%                        lin_vel(k,i) = norm(data.vel(:,k,i));
%                    end
                       
                
                    subplot(size(plot_coord,1),min(data.number_robots,max_number_subplot_columns),i);
                    plot([0:data.N_opt-1],data.traj.acc(:,i),'o'); grid;
                   % plot([0:data.N_opt-1],lin_vel(:,i)); grid;
                    xlim([0 data.N_opt]); ylim([-1 1]);
                    xticks([0:1:data.N_opt]);
                    %yticks([0:60:360]);
                    xlabel('Time (s)'); ylabel('Acceleration (m/s^2)');
                    title("Robot "+i)
            end
            
            
            
        end

        %%%%%%%%%
        function data = BuildFileForStorage(obj)
            data.traj.pos = obj.pos;
            data.traj.orientation = obj.orientation;
            data.base.pos = obj.mip.base.pos;
            data.number_robots = obj.mip.number_robots;
            data.polytopes = obj.scenario.polytopes;
            data.base.orient_index = obj.mip.base.orient_index;
            data.traj.lin_vel = obj.lin_vel;
            data.traj.acc = obj.acc;
            data.number_obs = obj.scenario.number_obs;
            data.number_tar = obj.scenario.number_tar;
            data.agents = obj.agents;
            data.B_con = value(obj.mip.opt_vars.B_con);
            data.B_con_base = value(obj.mip.opt_vars.B_con_base);
            data.index = obj.mip.index;
            data.N_opt = obj.N_opt;
            data.B_orient = value(obj.mip.opt_vars.B_orient);
        end
   
        
    end % end methods
end % end class

