# Motion Planning of Nonholonomic Robots with Visual Light Communication Connectivity

## Introduction

This repository contains the MATLAB code for the motion planning algorithm proposed in the paper "Mixed-Integer Motion Planning for Nonholonomic Robots under Visible Light Communication Constraints".
## Dependencies

- MATLAB (functional on version R2020b)
- YALMIP R20210331 (https://yalmip.github.io/)
- Multi-Parametric Toolbox R2011a (https://www.mpt3.org/)

## Instructions

Install MPT first and YALMIP will be automatically downloaded and installed. To modify the scenario and robot parameters, edit scenarios/TB3_BigField_1.m. To modify the optimization problem using the MIPClass. To run the optimization, use the script PlanTrajectories.m.


