function plot_comparisons()

%n_files = length(dir(['./data/*.mat']));
n_files = 150;

mksize = 80;
%opt_time_axes = axes;

% Costs
cost_fig = figure; hold on; grid;
i=0;
for j = 1 : n_files
    load('./data/data_'+string(j))
    
    if data.results_minlp.cost > 0.01
        i = i + 1;
        % Load cost
        cost_minlp(i) = data.results_minlp.cost;
        cost_milp_deg10(i) = data.results_milp.deg10.cost;
        cost_milp_deg15(i) = data.results_milp.deg20.cost;
        cost_milp_deg30(i) = data.results_milp.deg30.cost;
        
        % Load optimization times
        opt_time_minlp(i) = data.results_minlp.elapsed_time;
        opt_time_milp_deg10(i) = data.results_milp.deg10.elapsed_time;
        opt_time_milp_deg15(i) = data.results_milp.deg20.elapsed_time;
        opt_time_milp_deg30(i) = data.results_milp.deg30.elapsed_time;
        
        
        scatter(cost_minlp(i),cost_milp_deg10(i),mksize,'o','MarkerFaceColor','r','MarkerFaceAlpha',0.35,'MarkerEdgeAlpha',0);
        scatter(cost_minlp(i),cost_milp_deg15(i),mksize,'o','MarkerFaceColor','g','MarkerFaceAlpha',0.35,'MarkerEdgeAlpha',0);
        scatter(cost_minlp(i),cost_milp_deg30(i),mksize,'o','MarkerFaceColor','b','MarkerFaceAlpha',0.35,'MarkerEdgeAlpha',0);
    else
       disp(j);
    end
end

% Full
plot([0 10],[0 10],'k--','linewidth',1.5);
xlim([5 8.5]); ylim([5 8.5]);
xticks([5:0.5:8.5]); yticks([5:0.5:8.5]);
exportgraphics(cost_fig,"cost_fig"+".pdf",'ContentType','vector')

% Limit case 1
cost_limitcase1_fig = figure; hold; grid;
scatter(cost_minlp,cost_milp_deg10,mksize,'o','MarkerFaceColor','r','MarkerFaceAlpha',0.35,'MarkerEdgeAlpha',0);
scatter(cost_minlp,cost_milp_deg15,mksize,'o','MarkerFaceColor','g','MarkerFaceAlpha',0.35,'MarkerEdgeAlpha',0);
scatter(cost_minlp,cost_milp_deg30,mksize,'o','MarkerFaceColor','b','MarkerFaceAlpha',0.35,'MarkerEdgeAlpha',0);
plot([6 6.3],[6 6.3],'k--','linewidth',1.5);
xlim([6 6.3]); ylim([6 6.3]);
xticks([6:0.1:6.3]); yticks([6:0.1:6.3]);
exportgraphics(cost_limitcase1_fig,"cost_limitcase1_fig"+".pdf",'ContentType','vector')

% Limit case 2
cost_limitcase2_fig = figure; hold; grid;
scatter(cost_minlp,cost_milp_deg10,mksize,'o','MarkerFaceColor','r','MarkerFaceAlpha',0.35,'MarkerEdgeAlpha',0);
scatter(cost_minlp,cost_milp_deg15,mksize,'o','MarkerFaceColor','g','MarkerFaceAlpha',0.35,'MarkerEdgeAlpha',0);
scatter(cost_minlp,cost_milp_deg30,mksize,'o','MarkerFaceColor','b','MarkerFaceAlpha',0.35,'MarkerEdgeAlpha',0);
plot([5 5.3],[5 5.3],'k--','linewidth',1.5);
xlim([5 5.21]); ylim([5 5.21]);
xticks([5:0.1:5.2]); yticks([5:0.1:5.2]);
exportgraphics(cost_limitcase2_fig,"cost_limitcase2_fig"+".pdf",'ContentType','vector')


% Statistics cost
disp("Mean cost MINLP: " + mean(cost_minlp));
ci_minlp= bootci(length(cost_minlp),@(x)mean(x),cost_minlp);
disp("Bootstrap CI estimates [low,up]: " + "[" + ci_minlp(1) + "," + ci_minlp(2) + "]");

disp("Mean cost MILP-36: " + mean(cost_milp_deg10));
ci_10deg = bootci(length(cost_milp_deg10),@(x)mean(x),cost_milp_deg10);
disp("Bootstrap CI estimates [low,up]: " + "[" + ci_10deg(1) + "," + ci_10deg(2) + "]");

disp("Mean cost MILP-24: " + mean(cost_milp_deg15));
ci_15deg = bootci(length(cost_milp_deg15),@(x)mean(x),cost_milp_deg15);
disp("Bootstrap CI estimates [low,up]: " + "[" + ci_15deg(1) + "," + ci_15deg(2) + "]");

disp("Mean cost MILP-12: " + mean(cost_milp_deg30));
ci_30deg = bootci(length(cost_milp_deg30),@(x)mean(x),cost_milp_deg30);
disp("Bootstrap CI estimates [low,up]: " + "[" + ci_30deg(1) + "," +ci_30deg(2) + "]");

disp("")
disp("-------------")
disp("")

%% Opt times
opt_time_fig = figure; hold on; grid;
scatter(opt_time_minlp,opt_time_milp_deg10,mksize,'o','MarkerFaceColor','r','MarkerFaceAlpha',0.35,'MarkerEdgeAlpha',0);
scatter(opt_time_minlp,opt_time_milp_deg15,mksize,'o','MarkerFaceColor','g','MarkerFaceAlpha',0.35,'MarkerEdgeAlpha',0);
scatter(opt_time_minlp,opt_time_milp_deg30,mksize,'o','MarkerFaceColor','b','MarkerFaceAlpha',0.35,'MarkerEdgeAlpha',0);

plot([-20 620],[-20 620],'k--','linewidth',1.5);
xticks([0:150:600]); yticks([0:150:600]);
xlim([-20 620]); ylim([-20 620]);
exportgraphics(opt_time_fig,"opt_time_fig"+".pdf",'ContentType','vector')

% Statistics opt time
disp("Mean optimziation time MINLP: " + mean(opt_time_minlp));
ci_minlp= bootci(length(opt_time_minlp),@(x)mean(x),opt_time_minlp);
disp("Bootstrap CI estimates [low,up]: " + "[" + ci_minlp(1) + "," + ci_minlp(2) + "]");

disp("Mean optimziation time MILP-36: " + mean(opt_time_milp_deg10));
ci_10deg = bootci(length(opt_time_milp_deg10),@(x)mean(x),opt_time_milp_deg10);
disp("Bootstrap CI estimates [low,up]: " + "[" + ci_10deg(1) + "," + ci_10deg(2) + "]");

disp("Mean optimziation time MILP-24: " + mean(opt_time_milp_deg15));
ci_15deg = bootci(length(opt_time_milp_deg15),@(x)mean(x),opt_time_milp_deg15);
disp("Bootstrap CI estimates [low,up]: " + "[" + ci_15deg(1) + "," + ci_15deg(2) + "]");

disp("Mean optimziation time MILP-12: " + mean(opt_time_milp_deg30));
ci_30deg = bootci(length(opt_time_milp_deg30),@(x)mean(x),opt_time_milp_deg30);
disp("Bootstrap CI estimates [low,up]: " + "[" + ci_30deg(1) + "," +ci_30deg(2) + "]");

failures = 150-length(opt_time_milp_deg10);
disp("Failures MINLP: " + string(failures));


