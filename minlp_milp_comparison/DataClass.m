classdef DataClass < handle
    % Display data class
    
    properties
        mip = [];
        scenario = [];
        N_opt = [];
        traj = [];
        colors = [];
        agents = [];
        pos = []
        lin_vel = []
        acc = []
        orientation = []
    end
    
    methods
        function obj = DataClass(mip)
            
            % Extract all variables from MIP object
            
            % Horizon
            obj.N_opt = find(value(mip.opt_vars.B_hor) == 1);
            
            % tra
                for k = 1 : obj.N_opt
                    obj.pos(:,k) = value(mip.opt_vars.pos(mip.index.pos(:,k)));
                    obj.lin_vel(k) = value(mip.opt_vars.lin_vel(mip.index.lin_vel(k)));
                    obj.acc(k) = value(mip.opt_vars.acc(mip.index.lin_vel(k)));
                    obj.orientation(k) = (2*pi/mip.number_angles)*(find(round(value(mip.opt_vars.B_orient(mip.index.B_orient(k,:),1)))==1)-1);
                end
            
            obj.mip = mip;
            obj.N_opt = find(value(mip.opt_vars.B_hor) == 1);
        end
        
       

        %%%%%%%%%
        function results = get_results(obj)
            results.traj.pos = obj.pos;
            results.traj.orientation = obj.orientation;
            results.traj.lin_vel = obj.lin_vel;
            results.traj.acc = obj.acc;
            results.N_opt = obj.N_opt;
        end
   
        
    end % end methods
end % end class

