function fig = plot_scenario(poly,pars)

fig = figure; hold;

% Ini agent pos
plot(pars.rx0,pars.ry0,'bo');

% Obstacles
for t = 1 : length(poly.obs)
    
    plot(Polyhedron(poly.obs{t}.P,poly.obs{t}.q),'alpha',1,'color','black'); % expanded
 %   plot(Polyhedron(poly.obs{t}.P,poly.obs{t}.q_orig),'alpha',0.5,'color','gray'); % orig
end


% Targets
for t = 1 : length(poly.tar)
  %  plot(Polyhedron(poly.tar{t}.P,poly.tar{t}.q_orig),'alpha',0.5); % original
    plot(Polyhedron(poly.tar{t}.P,poly.tar{t}.q),'alpha',1); % shrinked
end
xlim([pars.rx_min-0.1,pars.rx_max+0.1]);
ylim([pars.ry_min-0.1,pars.ry_max+0.1]);

end

