function p =  plot_minlp_results(results)


disp("RX: " + results.RX)
disp("RY: " + results.RY)
disp("B_hor: " + results.B_hor)
disp("PSI: " + results.PSI*180/pi)
disp("PSI_INC: " + results.PSI_INC*180/pi)
disp("KSI: " + results.KSI)
disp("SIGMA: " + results.SIGMA )
disp("ACC: " + results.ACC)
disp("B_tar1: " + results.B_tar1)
%disp("B_tar2: " + results.B_tar2)

N_opt = find(results.B_hor==1);
p = plot(results.RX(1:N_opt),results.RY(1:N_opt),'DisplayName','NLMIP'); 

% % Obstacles
% for t = 1 : length(poly.obs)
%     
%     plot(Polyhedron(poly.obs{t}.P,poly.obs{t}.q),'alpha',1,'color','black'); % expanded
%  %   plot(Polyhedron(poly.obs{t}.P,poly.obs{t}.q_orig),'alpha',0.5,'color','gray'); % orig
% end
% 
% 
% % Targets
% for t = 1 : length(poly.tar)
%   %  plot(Polyhedron(poly.tar{t}.P,poly.tar{t}.q_orig),'alpha',0.5); % original
%     plot(Polyhedron(poly.tar{t}.P,poly.tar{t}.q),'alpha',1); % shrinked
% end
% xlim([pars.rx_min-0.1,pars.rx_max+0.1]);
% ylim([pars.ry_min-0.1,pars.ry_max+0.1]);

end

