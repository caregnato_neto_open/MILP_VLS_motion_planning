function save_data(results_minlp,results_milp,poly,pars,elapsed_time,cost,n_sols)

results_minlp.elapsed_time = elapsed_time;
results_minlp.cost = cost;

data.poly = poly;
data.pars = pars;
data.results_minlp = results_minlp;
data.results_milp.deg30 = results_milp.deg30;
data.results_milp.deg20 = results_milp.deg15;
data.results_milp.deg10 = results_milp.deg10;

save("./data/data_"+string(n_sols),'data')


end

