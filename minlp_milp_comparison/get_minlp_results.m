function results = get_minlp_results(opti,opt_vars)

results.RX = opti.value(opt_vars.RX);
results.RY = opti.value(opt_vars.RY);
results.B_hor = opti.value(opt_vars.B_hor); 
results.PSI = opti.value(opt_vars.PSI);
results.PSI_INC = opti.value(opt_vars.PSI_INC);
results.KSI = opti.value(opt_vars.KSI);
results.SIGMA = opti.value(opt_vars.SIGMA);
results.ACC = opti.value(opt_vars.ACC);
results.B_tar1 = opti.value(opt_vars.B_tar{1});

for o = 1 : length(opt_vars.B_obs)
    results.B_obs{o} = opti.value(opt_vars.B_obs{o});
end



end

