function scenario_struct = GenRngScenObsSide(params)


number_obs_sides = params.number_obs_sides;
n_obstacles = params.number_obstacles;
scenario_struct.number_agents = params.number_agents;

%% Environment
op_region_width = 2;
op_region_length = 2;
op_region_center = [0,0];

%number_obs_sides = 5;
number_obs_vertices = number_obs_sides;

% Operation region ( static )
scenario_struct.polytopes.op_region.P = [eye(2);-eye(2)];
scenario_struct.polytopes.op_region.q = [op_region_center(1) + op_region_length/2;
    op_region_center(2) + op_region_width/2;
    op_region_center(1) + op_region_length/2;
    op_region_center(2) + op_region_width/2];

%% Agent initial position (rng)
    agent_region_start_x = -op_region_length;
    agent_region_end_x = -op_region_length+0.5;
    rng_agent_pos = [agent_region_start_x + (agent_region_end_x-agent_region_start_x)*rand(1,1);
        (-op_region_width)/2 + (op_region_width)*rand(1,1)];
    scenario_struct.rng_agent_pos = rng_agent_pos;


%% Target (rng position)
    tar_len = .25;
    tar_wid = .25;
    tar_region_start_x = op_region_length-0.5+tar_len/2;
    tar_region_end_x = op_region_length-tar_len/2;
    
    rng_tar_pos = [tar_region_start_x + (tar_region_end_x-tar_region_start_x)*rand(1,1);
        (-op_region_width+tar_wid/2)/2 + (op_region_width-tar_wid/2)*rand(1,1)];
    
%     rng_tar_pos = [(-op_region_length+tar_len/2)/2 + (op_region_length-tar_len/2)*rand(1,1);
%         (-op_region_width+tar_wid/2)/2 + (op_region_width-tar_wid/2)*rand(1,1)];
    


scenario_struct.polytopes.tar{1}.P = [eye(2);-eye(2)];

scenario_struct.polytopes.tar{1}.q = [rng_tar_pos(1)+tar_len/2; 
    rng_tar_pos(2)+tar_wid/2; 
    -(rng_tar_pos(1)-tar_len/2); 
    -(rng_tar_pos(2)-tar_wid/2)];


poly_tar = Polyhedron(scenario_struct.polytopes.tar{1}.P,scenario_struct.polytopes.tar{1}.q);

% plot(Polyhedron(scenario_struct.polytopes.tar.P,scenario_struct.polytopes.tar.q));
% xlim([-1.5,1.5]);ylim([-1.5,1.5]);

%% Obstacles ( random )

obs_max_length = 1.5;
obs_max_width = 1.5;

obs_region_length = 1;
obs_region_width = 3;
obs_region_center = [0,0];


idx = 1;

%figure; hold; xlim([-1.5 1.5]); ylim([-1.5 1.5]);
while idx <= n_obstacles
    no_intersection = false;
    
    % Determines random dimensions of a box containing the ellipse
    ellipse_dimensions{idx} = [rand(1,1)*obs_max_length,rand(1,1)*obs_max_width];
    
    % Randomly determines ellipse's orientation
    ellipse_orientation{idx} = 2*pi*rand(1,1);
    
    
    % Builds random rotated ellipse
    angles = [0:0.01:2*pi];
    
    unrotated_ellipse = [ellipse_dimensions{idx}(1)/2.*cos(angles); ellipse_dimensions{idx}(2)/2.*sin(angles)];
        
    
    R = [cos(ellipse_orientation{idx}) sin(ellipse_orientation{idx});-sin(ellipse_orientation{idx}) cos(ellipse_orientation{idx})];
    for j = 1 : length(unrotated_ellipse); ellipse{idx}(:,j) = R*unrotated_ellipse(:,j); end
    
    % Randomly translates ellipse
    x_sup_bound = obs_region_length/2-max(ellipse{idx}(1,:));
    x_inf_bound = -(obs_region_length/2) - min(ellipse{idx}(1,:));
    y_sup_bound = obs_region_width/2-max(ellipse{idx}(2,:));
    y_inf_bound = -(obs_region_width/2) - min(ellipse{idx}(2,:));
    ellipse_center{idx} = [x_inf_bound + (-x_inf_bound + x_sup_bound)*rand(1,1);
        y_inf_bound + (-y_inf_bound + y_sup_bound)*rand(1,1)];
    
    ellipse{idx}(1,:) = ellipse{idx}(1,:) + ellipse_center{idx}(1);
    ellipse{idx}(2,:) = ellipse{idx}(2,:) + ellipse_center{idx}(2);
    
    % Randomly samples the ellipse to determine obstacle vertices
    ellipse_sample_index = randi([1 length(angles)],number_obs_vertices,1);
    vertices = ellipse{idx}(:,ellipse_sample_index)';
    
    % Generates polygon
    G = Polyhedron(vertices);
   % G = G.minHRep;
    scenario_struct.polytopes.obs{idx}.P = G.A;
    scenario_struct.polytopes.obs{idx}.q = G.b;
    
    % Check if there is intersection between new obstacle and target
    poly1 = Polyhedron(scenario_struct.polytopes.obs{idx}.P,scenario_struct.polytopes.obs{idx}.q);
    poly4 = poly1.intersect(poly_tar); % Checks if intersects with target
    
    if ~poly4.isEmptySet
        idx = idx - 1; % no good try again
    else
        % Checks if there is intersection between new obstacles
        for j = idx-1: -1 : 1
            poly1 = Polyhedron(scenario_struct.polytopes.obs{idx}.P,scenario_struct.polytopes.obs{idx}.q);
            poly2 = Polyhedron(scenario_struct.polytopes.obs{j}.P,scenario_struct.polytopes.obs{j}.q);
            poly3 = poly1.intersect(poly2);
            poly4 = poly1.intersect(poly_tar); % Checks if intersects with target

            if ~poly3.isEmptySet 
                idx = idx - 1; % no good try again
            else
                no_intersection = true;
                % Checks if it has the correct number of sides, sometimes it has less
                % sides and this breaks the code
                if length(G.A) ~= number_obs_sides
                    idx = idx - 1;
                end

            end

        end
    end
    idx = idx + 1;
    
end

scenario_struct.number_obs = length(ellipse_center);

%% Agents
% Number of agents

% All agents are equal in this case
A = [0 1 0 0; 0 0 0 0;0 0 0 1; 0 0 0 0];
scenario_struct.nx = size(A,1);
B = [0 0; 1 0;0 0;0 1];
scenario_struct.nu = size(B,2);
C = [1 0 0 0;0 0 1 0];
scenario_struct.ny = size(C,1);
D = zeros(scenario_struct.ny,scenario_struct.nu);
Ts = 1; % seconds;

i = 1;
while i <= scenario_struct.number_agents
    
    % Randomly selects initial position
    rng_pos = [(-obs_region_length)/2 + (obs_region_length)*rand(1,1);
        (-obs_region_width)/2 + (obs_region_width)*rand(1,1)];
    
    scenario_struct.agents{idx}.x0 = [rng_pos(1);0;rng_pos(2);0];
    
    % Dynamics
    scenario_struct.agents{idx}.dyn = c2d(ss(A,B,C,D),Ts);
    
    % Body polytope
    scenario_struct.polytopes.agent_body{idx}.P = [eye(2);-eye(2)];
    scenario_struct.polytopes.agent_body{idx}.q = [0;0;0;0];
    
    % Verifies if inside obstalce
    for j = 1 : scenario_struct.number_obs
        
        if all(scenario_struct.polytopes.obs{j}.P*rng_pos <= scenario_struct.polytopes.obs{j}.q)
            i = i - 1;
        end
        
    end
    i = i + 1;
end

end

