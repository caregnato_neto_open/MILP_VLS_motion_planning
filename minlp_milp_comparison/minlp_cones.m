double_support_interpolation = @(t, T_ds) min(1.0, max(0.0, t / T_ds));
import casadi.*

% regions_selection = 'fixed';
regions_selection = 'mixed_integer';

%% Parameters
F = 4;
C = 100;
R = 2;
delta = 0.01;
M = 100.0;

g = 9.81;
h = 0.8;
eta = sqrt(g / h);
foot_size = [0.25; 0.10];
d = 0.4;
t_change = 0.1;

delta_x_min = -0.2;
delta_x_max = 0.3;
delta_y_bar = 0.05;
y_displacement = 0.25;
delta_z_min = -0.05;
delta_z_max = 0.05;
delta_theta_min = -0.5;
delta_theta_max = 0.5;
t_ds_min = 0.2;
t_ds_max = 0.2;
t_ss_min = 0.3;
t_ss_max = 0.6;

t_elapsed = 0.0;
b_ls = 1;

x0 = 0.0;
y0 = 0.0;
z0 = 0.0;
theta0 = 0.0;

xc = 0.0;
dxc = 0.0;
xz = 0.0;
xu = xc + dxc / eta;

yc = 0.0;
dyc = 0.0;
yz = 0.0;
yu = yc + dyc / eta;

zc = h;
dzc = 0.0;
zz = 0.0;
zu = zc + dzc / eta;

X_ref = [0.1; 0.3; 0.5; 0.6];
Y_ref = [-0.25; 0.0; -0.25; 0.0];
Z_ref = [0.0; 0.0; 0.05; 0.05];
Theta_ref = [0.0; 0.0; 0.0; 0.0];
T_ds_ref = 0.1 * ones(4, 1);
T_ss_ref = 0.3 * ones(4, 1);
B_ref = zeros(F * R, 1);
B_ref(1) = 1;
B_ref(R + 1) = 1;
B_ref(2 * R + 2) = 1;
B_ref(3 * R + 2) = 1;

%% Initializations

assert(size(X_ref, 1) == F);
assert(size(Y_ref, 1) == F);
assert(size(Z_ref, 1) == F);
assert(size(Theta_ref, 1) == F);
assert(size(T_ds_ref, 1) == F);
assert(size(T_ss_ref, 1) == F);
assert(size(B_ref, 1) == F * R);

X_min = repmat(delta_x_min, F, 1);
X_max = repmat(delta_x_max, F, 1);
Y_min = repmat(-delta_y_bar, F, 1) + y_displacement * (-1).^(b_ls+(1:F)-1)';
Y_max = repmat(delta_y_bar, F, 1) + y_displacement * (-1).^(b_ls+(1:F)-1)';
Z_min = repmat(delta_z_min, F, 1);
Z_max = repmat(delta_z_max, F, 1);
Theta_min = repmat(delta_theta_min, F, 1);
Theta_max = repmat(delta_theta_max, F, 1);
T_ds_min = repmat(t_ds_min, F, 1);
T_ds_max = repmat(t_ds_max, F, 1);
T_ss_min = repmat(t_ss_min, F, 1);
T_ss_max = repmat(t_ss_max, F, 1);

P = delta * tril(ones(C, C));
p = ones(C, 1);

A = (1.0 - exp(-eta * delta)) / eta * exp(-eta * (0:delta:(C-1)*delta));
A

opti = casadi.Opti();

X = opti.variable(F);
Y = opti.variable(F);
Z = opti.variable(F);
Theta = opti.variable(F);
T_ds = opti.variable(F);
T_ss = opti.variable(F);

opti.set_initial(X, X_ref);
opti.set_initial(Y, Y_ref);
opti.set_initial(Z, Z_ref);
opti.set_initial(Theta, Theta_ref);
opti.set_initial(T_ds, T_ds_ref);
opti.set_initial(T_ss, T_ss_ref);

X_bar = [x0; X(1:end-1)];
Y_bar = [y0; Y(1:end-1)];
Z_bar = [z0; Z(1:end-1)];
Theta_bar = [theta0; Theta(1:end-1)];

X1 = X_ref(1);
Y1 = Y_ref(1);
Z1 = Z_ref(1);
Theta1 = Theta_ref(1);
T_ds1 = T_ds_ref(1);
T_ss1 = T_ss_ref(1);

t_sum = 0.0;
Xz_mc = x0 + (X(1) - x0) * double_support_interpolation(t_elapsed + P * p - t_sum, T_ds(1));
Yz_mc = y0 + (Y(1) - y0) * double_support_interpolation(t_elapsed + P * p - t_sum, T_ds(1));
Zz_mc = z0 + (Z(1) - z0) * double_support_interpolation(t_elapsed + P * p - t_sum, T_ds(1));
for j=1:F-1
    t_sum = t_sum + T_ds(j) + T_ss(j);
    Xz_mc = Xz_mc + (X(j+1) - X(j)) * double_support_interpolation(t_elapsed + P * p - t_sum, T_ds(j+1));
    Yz_mc = Yz_mc + (Y(j+1) - Y(j)) * double_support_interpolation(t_elapsed + P * p - t_sum, T_ds(j+1));
    Zz_mc = Zz_mc + (Z(j+1) - Z(j)) * double_support_interpolation(t_elapsed + P * p - t_sum, T_ds(j+1));
end

cost = (X_ref - X)' * (X_ref - X) + (Y_ref - Y)' * (Y_ref - Y) +...
       (Z_ref - Z)' * (Z_ref - Z) +...
       10 * (Theta_ref - Theta)' * (Theta_ref - Theta) +...
       10 * (T_ds_ref - T_ds)' * (T_ds_ref - T_ds) +...
       10 * (T_ss_ref - T_ss)' * (T_ss_ref - T_ss);

opti.subject_to(X_min <= cos(Theta_bar) .* (X - X_bar) + sin(Theta_bar) .* (Y - Y_bar) <= X_max);
opti.subject_to(Y_min <= -sin(Theta_bar) .* (X - X_bar) + cos(Theta_bar) .* (Y - Y_bar) <= Y_max);
opti.subject_to(Z_min <= Z - Z_bar <= Z_max);
opti.subject_to(Theta_min <= Theta - Theta_bar <= Theta_max);
opti.subject_to(T_ds_min <= T_ds <= T_ds_max);
opti.subject_to(T_ss_min <= T_ss <= T_ss_max);

opti.subject_to(-A * inv(P) * (p * xz + d) + xz - xu <= -A * inv(P) * Xz_mc <= -A * inv(P) * (p * xz - d) + xz - xu);
opti.subject_to(-A * inv(P) * (p * yz + d) + yz - yu <= -A * inv(P) * Yz_mc <= -A * inv(P) * (p * yz - d) + yz - yu);
opti.subject_to(-A * inv(P) * (p * zz + d) + zz - zu + g / eta^2 <= -A * inv(P) * Zz_mc <= -A * inv(P) * (p * zz - d) + zz - zu + g / eta^2);

opti.subject_to(X(1) == X1);
opti.subject_to(Y(1) == Y1);
opti.subject_to(Z(1) == Z1);
opti.subject_to(Theta(1) == Theta1);

if t_elapsed > T_ds1 + T_ss1 - t_change
    disp('Second footstep (first to be decided) cannot be changed!')
    X2 = X_ref(2);
    Y2 = Y_ref(2);
    Z2 = Z_ref(2);
    Theta2 = Theta_ref(2);
    opti.subject_to(X(2) == X2);
    opti.subject_to(Y(2) == Y2);
    opti.subject_to(Z(2) == Z2);
    opti.subject_to(Theta(2) == Theta2);
end

if t_elapsed > T_ds1
    disp('Robot is in single support!')
    opti.subject_to(T_ds(1) == T_ds1);
end

if strcmp(regions_selection, 'fixed')
    xa = 0.45;
    ya = -0.1;
    xb = 0.55;
    yb = 0.1;
    
    vertices = [xa ya; xb ya; xb yb; xa yb];
    poly = Polyhedron(vertices);
    poly.computeHRep();
    
    opti.subject_to(poly.A * [X(3); Y(3)] <= poly.b);
    opti.subject_to(poly.A * [X(4); Y(4)] <= poly.b);
    
    % Restricting the heights of the footsteps
    z12 = 0.0;
    z34 = 0.05;
    
    opti.subject_to(Z(1) == z12);
    opti.subject_to(Z(2) == z12);
    opti.subject_to(Z(3) == z34);
    opti.subject_to(Z(4) == z34);
elseif strcmp(regions_selection, 'mixed_integer')
    B = opti.variable(F * R);

    opti.set_initial(B, B_ref);

    xa = -5.0;
    ya = -5.0;
    xb = 0.45;
    yb = 5.0;
    
    vertices1 = [xa ya; xb ya; xb yb; xa yb];
    polys(1) = Polyhedron(vertices1);
    polys(1).computeHRep();
    
    xa = 0.45;
    ya = -0.1;
    xb = 0.55;
    yb = 0.1;
    
    vertices2 = [xa ya; xb ya; xb yb; xa yb];
    polys(2) = Polyhedron(vertices2);
    polys(2).computeHRep();
    
    z_reg = [0.0; 0.05];
    
    for j=1:F
        sum_B = 0;
        for r=1:R
            sum_B = sum_B + B((j - 1) * R + r);
            opti.subject_to(polys(r).A * [X(j); Y(j)] - (1.0 - B((j - 1) * R + r)) * M <= polys(r).b);
            opti.subject_to(-Z(j) - (1.0 - B((j - 1) * R + r)) * M <= z_reg(r));
            opti.subject_to(Z(j) - (1.0 - B((j - 1) * R + r)) * M <= z_reg(r));
        end
        opti.subject_to(sum_B == 1);
    end

    discrete = zeros(6 * F + F * R, 1);
    discrete(6*F+1:6*F+F*R) = ones(F * R, 1);

    opts.discrete = discrete;

    cost = cost + (B_ref - B)' * (B_ref - B);
end

opti.minimize(cost);

opts.expand = true;
% opts.verbose = true;

opti.solver('bonmin', opts);
% test_value_or_reference(opti);

num_runs = 100;
solver_times = zeros(num_runs, 1);
for i=1:length(solver_times)
    tic;
    sol = opti.solve();
    solver_times(i) = toc;
end

X_sol = opti.value(X)
Y_sol = opti.value(Y)
Z_sol = opti.value(Z)
Theta_sol = opti.value(Theta)
T_ds_sol = opti.value(T_ds)
T_ss_sol = opti.value(T_ss)

fprintf('Solver Time: mean: %f; std: %f\n', mean(solver_times), std(solver_times));



function test_value_or_reference(opti, opts)
    opts.expand = true;
    % opts.verbose = true;
    
    opti.solver('bonmin', opts);
end