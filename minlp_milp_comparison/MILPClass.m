classdef MILPClass < handle
    % MILP class
    
    properties
        trans_dof = [];
        number_tar = [];
        number_obs = [];
        number_robots = [];
        number_obs_sides = [];
        number_body_sides = [];
        number_angles = [];
        number_cone_sides = [];
        N = [];
        BigM = [];
        opt_vars = [];
        constraints = [];
        index = [];
        cost = []
        gamma_incr = [];
        gamma_a = [];
        beta = [];
        base = [];
        B_obs_base = [];
        Ts = []
        
    end
    
    methods
        function MILP = MILPClass(poly,n_angles)
            
            % Auxiliary vars
            MILP.trans_dof = 2;
            MILP.number_robots = 1;
            MILP.number_obs = length(poly.obs);
            MILP.number_tar = length(poly.tar);
            MILP.number_obs_sides = 4;
            

            % Optimization parameters
            MILP.N =  6;
            MILP.BigM = 100;
            MILP.gamma_incr = 0.1; 
            MILP.gamma_a = 0.1; 
            MILP.Ts = 1; % sampling period
            MILP.number_angles = n_angles;
            
            % Real-valued optimization variables
            MILP.opt_vars.pos = sdpvar((MILP.N+1)*MILP.number_robots*MILP.trans_dof,1);
            MILP.opt_vars.lin_vel = sdpvar((MILP.N+1)*MILP.number_robots,1);
            MILP.opt_vars.acc = sdpvar((MILP.N+1)*MILP.number_robots,1);
            MILP.opt_vars.abs_acc = sdpvar((MILP.N+1)*MILP.number_robots,1);
            MILP.opt_vars.abs_orient_diff = sdpvar((MILP.N)*(MILP.number_robots),1);   
            MILP.opt_vars.B_orient_pen_1 = binvar(MILP.N*MILP.number_robots,1);
            MILP.opt_vars.B_orient_pen_2 = binvar(MILP.N*MILP.number_robots,1);
          
            % Binary variables
            MILP.opt_vars.B_hor = binvar(MILP.N+1,1);
            MILP.opt_vars.B_tar = binvar((MILP.N+1)*MILP.number_tar,1);
            MILP.opt_vars.B_obs = binvar((MILP.N+1)*MILP.number_robots*MILP.number_obs*MILP.number_obs_sides,1);
            MILP.opt_vars.B_orient = binvar(MILP.number_angles*(MILP.N+1)*(MILP.number_robots),1);
        end
        
        function BuildOptVarIndexes(MILP)
            
            counter = 0;
            counter_obs = 0;
            counter_orient = 0;
            counter_tar = 0;
            counter_abs_orient_diff = 0;
          
            
         
                for k = 1 : MILP.N+1
                    
                    counter = counter + 1;
                    
                    % linear velocity (nu) --------------------------------
                    MILP.index.lin_vel(k) = counter;
                    
                    
                    % pos -------------------------------------------------
                    MILP.index.pos(:,k) = (counter-1)*MILP.trans_dof+1:counter*MILP.trans_dof;
                    
                    
                    % obs bin ---------------------------------------------
                    for j = 1 : MILP.number_obs
                        for n = 1 : MILP.number_obs_sides
                            counter_obs =  counter_obs + 1;
                            MILP.index.B_obs(n,k,j) = counter_obs; 
                        end
                    end
                    
                    
                    % orient bin ------------------------------------------
                    for p = 1 : MILP.number_angles
                        counter_orient = counter_orient+1;
                        MILP.index.B_orient(k,p) = counter_orient;
                    end
                end
                
                
          

            for k = 1 : MILP.N+1
                 % target bin ----------------------------------------
                for j = 1 : MILP.number_tar
                    counter_tar = counter_tar + 1;
                    MILP.index.B_tar(k,j) = counter_tar;
                end
                
                % Orient increment ----------------------------------------
                
                    counter_abs_orient_diff = counter_abs_orient_diff + 1;
                    MILP.index.abs_orient_diff(k) = counter_abs_orient_diff;
                
                
            end
            
        end
       
        % Eq. (3) and (4). and const. (8), (9), (10), (11) and (12)
        function BuildNonlinearDynamicsConstraints(MILP,pars)
            
            angles = [-pi:2*pi/MILP.number_angles:pi];
            
            MILP.constraints.dyn = [];
            MILP.constraints.initial_condition = [];
            for i = 1 : MILP.number_robots
                
                
                % Initial  conditions
                initial_robot_pos = [pars.rx0;pars.ry0];
                MILP.constraints.initial_condition = [MILP.constraints.initial_condition,
                    MILP.opt_vars.pos(MILP.index.pos(:,1)) == initial_robot_pos,
                    MILP.opt_vars.lin_vel(MILP.index.lin_vel(1)) == 0,
                    ];
                
                for k = 1 : MILP.N
                    
                    % var assignments for better readibility
                    rxk = MILP.opt_vars.pos(MILP.index.pos(1,k));
                    rxk_next = MILP.opt_vars.pos(MILP.index.pos(1,k+1));
                    ryk = MILP.opt_vars.pos(MILP.index.pos(2,k));
                    ryk_next = MILP.opt_vars.pos(MILP.index.pos(2,k+1));
                    acc = MILP.opt_vars.acc(MILP.index.lin_vel(k)); % using index from linvel (equal)
                    linvel_k = MILP.opt_vars.lin_vel(MILP.index.lin_vel(k));
                    linvel_next = MILP.opt_vars.lin_vel(MILP.index.lin_vel(k+1));
                    
                    % Velocity model 
                    MILP.constraints.dyn = [MILP.constraints.dyn,
                        linvel_next <= linvel_k + acc*MILP.Ts + sum(MILP.opt_vars.B_hor(1:k))*MILP.BigM,
                        -linvel_next <= -(linvel_k + acc*MILP.Ts) + sum(MILP.opt_vars.B_hor(1:k))*MILP.BigM];
                    
                    % Position model 
                    for n = 1 : MILP.number_angles
                        B_orient = MILP.opt_vars.B_orient(MILP.index.B_orient(k,n));
                        
                        MILP.constraints.dyn = [MILP.constraints.dyn,
                            rxk_next <= rxk+(linvel_k*MILP.Ts+acc*(MILP.Ts^2/2))*round(cos(angles(n)),4)+(1-B_orient)*MILP.BigM+sum(MILP.opt_vars.B_hor(1:k))*MILP.BigM,
                            ryk_next <= ryk+(linvel_k*MILP.Ts+acc*(MILP.Ts^2/2))*round(sin(angles(n)),4)+(1-B_orient)*MILP.BigM+sum(MILP.opt_vars.B_hor(1:k))*MILP.BigM];
                        
                        MILP.constraints.dyn = [MILP.constraints.dyn,
                            -rxk_next <= -rxk-(linvel_k*MILP.Ts+acc*(MILP.Ts^2/2))*round(cos(angles(n)),4)+(1-B_orient)*MILP.BigM+sum(MILP.opt_vars.B_hor(1:k))*MILP.BigM,
                            -ryk_next <= -ryk-(linvel_k*MILP.Ts+acc*(MILP.Ts^2/2))*round(sin(angles(n)),4)+(1-B_orient)*MILP.BigM+sum(MILP.opt_vars.B_hor(1:k))*MILP.BigM];
                    end
                end
            end
            
            % This constraints imposes that the robot must be oriented
            % towards some of the predefined orientations 
            MILP.constraints.orient_binary = [];

                for k = 1 : MILP.N + 1
                    MILP.constraints.orient_binary = [MILP.constraints.orient_binary,
                        sum(MILP.opt_vars.B_orient(MILP.index.B_orient(k,:))) == 1];
                end
           
        end
        
        % Const. (33b), (33c), and partially (33d)
        function BuildPositionAndInputConstraints(MILP,pars)
   
            
            MILP.constraints.acceleration = [];
            MILP.constraints.velocity = [];
            MILP.constraints.position = [];
            
            
            for k = 1 : MILP.N
                for i = 1 : MILP.number_robots
                    
                    % Var assignments
                    acc = MILP.opt_vars.acc(MILP.index.lin_vel(k));
                    lin_vel = MILP.opt_vars.lin_vel(MILP.index.lin_vel(k+1));
                    pos = MILP.opt_vars.pos(MILP.index.pos(:,k+1));
                    
                    % Acceleration constraint
                    MILP.constraints.acceleration = [MILP.constraints.acceleration,
                        acc <= pars.lin_acc_max + sum(MILP.opt_vars.B_hor(1:k))*MILP.BigM,
                        acc >= -pars.lin_acc_max - sum(MILP.opt_vars.B_hor(1:k))*MILP.BigM
                        ];
                    
                    % Velocity constraints
                    MILP.constraints.velocity = [MILP.constraints.velocity,
                        lin_vel <= pars.lin_vel_max + sum(MILP.opt_vars.B_hor(1:k))*MILP.BigM,
                                                lin_vel >= 0 - sum(MILP.opt_vars.B_hor(1:k))*MILP.BigM];
                    
                    % Position constraints
                      MILP.constraints.position = [MILP.constraints.position,
                        pos <= [pars.rx_max;pars.ry_max] + ones(2,1)*sum(MILP.opt_vars.B_hor(1:k))*MILP.BigM,
                        -pos <= -[pars.rx_min;pars.ry_min] + ones(2,1)*sum(MILP.opt_vars.B_hor(1:k))*MILP.BigM];
                end
            end
        end    
        
        % Const. (33e) and Const. (33f)
        function BuildTargetConstraints(MILP,poly)
            
            MILP.constraints.targets = [];
            MILP.constraints.target_hor_eq  = [];
            MILP.constraints.tar_limit = [];
            MILP.constraints.no_reward_after_horizon = [];
            
            % Target entry
            for k = 1 : MILP.N + 1
                    pos = MILP.opt_vars.pos(MILP.index.pos(:,k,MILP.number_robots));
                    
                    for t = 1 : MILP.number_tar
                        B_tar = MILP.opt_vars.B_tar(MILP.index.B_tar(k,t));
                        MILP.constraints.targets = [MILP.constraints.targets,
                            poly.tar{t}.P*pos <= poly.tar{t}.q + ones(length(poly.tar{t}.q),1)*(1-B_tar)*MILP.BigM];
                    end
            end
            
            % Target binary conditions
            for k = 1 : MILP.N+1
                % Horizon is equal to sum of last target entry binary
                MILP.constraints.target_hor_eq = [MILP.constraints.target_hor_eq,
                     sum(MILP.opt_vars.B_tar(MILP.index.B_tar(1:k,1))) >= MILP.opt_vars.B_hor(k)];
            end
            
            % Limit target collection
            for t = 1 : MILP.number_tar
                aux = [];
                for k = 1 : MILP.N + 1
                    aux = [aux;sum(MILP.opt_vars.B_tar(MILP.index.B_tar(k,t)))];
                end
                MILP.constraints.tar_limit = [MILP.constraints.tar_limit, sum(aux)<=1];
            end
           
            MILP.constraints.horizon = [sum(MILP.opt_vars.B_hor) == 1];
            
        end
        
        % Partially const. (33d)
        function BuildObstacleAvoidanceConstraints(MILP,poly)
            
            MILP.constraints.obstacles = [];
            MILP.constraints.obstacles_binary = [];
            MILP.constraints.obstacles_corner_cutting = [];
            for i = 1 : MILP.number_robots
                for k = 1 : MILP.N+1
                    
                    pos = MILP.opt_vars.pos(MILP.index.pos(:,k));
                        
                    if k <= MILP.N
                         pos_next = MILP.opt_vars.pos(MILP.index.pos(:,k+1));
                    end
                    
                    for j = 1 : MILP.number_obs
                        B_obs = MILP.opt_vars.B_obs(MILP.index.B_obs(:,k,j));
                        
                        % Obs entry constraint
                        MILP.constraints.obstacles = [MILP.constraints.obstacles,
                            -poly.obs{j}.P*pos <= -poly.obs{j}.q+(ones(4,1)-B_obs)*MILP.BigM+ones(4,1)*sum(MILP.opt_vars.B_hor(1:k-1))*MILP.BigM];
                        
                        % Corner cutting
                        if k <= MILP.N
                            MILP.constraints.obstacles_corner_cutting = [MILP.constraints.obstacles_corner_cutting,
                                -poly.obs{j}.P*pos_next <= -poly.obs{j}.q+(ones(4,1)-B_obs)*MILP.BigM+ones(4,1)*sum(MILP.opt_vars.B_hor(1:k))*MILP.BigM];
                        end
                        
                        % obs binary constraint
                        MILP.constraints.obstacles_binary = [MILP.constraints.obstacles_binary,
                            sum(B_obs) >= 1];
                    end
                end
            end
            
            
        end
        
        % Const. (30)
        function BuildOrientationIncrementConstraint(MILP)
            
            MILP.constraints.orient_increment = [];
            for i = 1 : MILP.number_robots
                for k = 2 : MILP.N+1
                    MILP.constraints.orient_increment = [MILP.constraints.orient_increment,
                    MILP.opt_vars.abs_orient_diff(MILP.index.abs_orient_diff(k-1)) <= pi/3];
                end
            end

        end
        
        % For the cost
        function BuildAccelerationAbsoluteValueConstraint(MILP)
            MILP.constraints.abs_acc = [-MILP.opt_vars.abs_acc <= MILP.opt_vars.acc <= MILP.opt_vars.abs_acc];
        end
        
        function BuildOrientationPenalization(MILP)
            
            MILP.constraints.abs_orient_diff = [];
            MILP.constraints.sense = [];
            
            angles = [-pi:2*pi/MILP.number_angles:pi];
            angles = angles(1:end-1); % ignore last entry as 0 = 2pi
            
            for i = 1 : MILP.number_robots
                for k = 2 : MILP.N+1
                    B_orient_pen_1 = MILP.opt_vars.B_orient_pen_1(MILP.index.abs_orient_diff(k-1));
                    B_orient_pen_2 = MILP.opt_vars.B_orient_pen_2(MILP.index.abs_orient_diff(k-1));
                    orient_diff =  angles*(MILP.opt_vars.B_orient(MILP.index.B_orient(k,:))-MILP.opt_vars.B_orient(MILP.index.B_orient(k-1,:)));
                    
                    MILP.constraints.sense = [MILP.constraints.sense,
                        orient_diff <= pi + B_orient_pen_1*MILP.BigM;
                        -orient_diff <= pi +  B_orient_pen_2*MILP.BigM;
                        B_orient_pen_1 + B_orient_pen_2 <= 1];
                    
                    MILP.constraints.abs_orient_diff = [MILP.constraints.abs_orient_diff,
                        -MILP.opt_vars.abs_orient_diff(MILP.index.abs_orient_diff(k-1)) <=  orient_diff - 2*pi*B_orient_pen_1 + 2*pi*B_orient_pen_2 <= MILP.opt_vars.abs_orient_diff(MILP.index.abs_orient_diff(k-1))];
                end
            end
        end
        
        % Cost
        function BuildCost(MILP)
            
            MILP.cost = [1:MILP.N+1]*MILP.opt_vars.B_hor + MILP.gamma_a*sum(MILP.opt_vars.abs_acc) + MILP.gamma_incr*sum(MILP.opt_vars.abs_orient_diff);
            
        end
        
    end % end methods
end % end class

