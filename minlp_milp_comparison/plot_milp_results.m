function p = plot_milp_results(results)

p = plot(results.traj.pos(1,1:results.N_opt),results.traj.pos(2,1:results.N_opt),'DisplayName','MILP');

end

