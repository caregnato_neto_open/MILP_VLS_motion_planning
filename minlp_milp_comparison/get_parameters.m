function pars = get_parameters()

%
pars.max_hor = 8; % number of translation DOF
pars.T = 1; % sampling period
pars.M = 100; % Big-M

% Bounds
pars.rx_max = 2;
pars.rx_min = -2;
pars.ry_max = 1.5;
pars.ry_min = -1.5;
pars.lin_vel_max = 1;
pars.lin_vel_min = 1.0000e-06;
pars.lin_acc_max = 1;
pars.lin_acc_min = -1;
pars.ori_max = 2*pi;
pars.ori_min = 1.0000e-06;
pars.ori_inc_max = pi/3;
pars.ori_inc_min = -pi/3;

% Initial conditions


pars.rx0 = -1.5;
pars.ry0 = -0.75;
%pars.psi0 = 0;
pars.ksi0 = 0;





end

