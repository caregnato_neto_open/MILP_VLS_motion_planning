function results_milp = solve_milp_approx(pars,poly,n_angles)


% Build problem
MILP = MILPClass(poly,n_angles);
MILP.BuildOptVarIndexes();

% Constraints
MILP.BuildNonlinearDynamicsConstraints(pars);
MILP.BuildPositionAndInputConstraints(pars);
MILP.BuildTargetConstraints(poly);
MILP.BuildObstacleAvoidanceConstraints(poly);
MILP.BuildAccelerationAbsoluteValueConstraint()
MILP.BuildOrientationIncrementConstraint();
MILP.BuildOrientationPenalization();

constraints = [
    MILP.constraints.dyn
    MILP.constraints.initial_condition
    MILP.constraints.position
    MILP.constraints.acceleration
    MILP.constraints.velocity
    MILP.constraints.abs_acc
    MILP.constraints.targets
    MILP.constraints.target_hor_eq
    MILP.constraints.horizon
    MILP.constraints.tar_limit
    MILP.constraints.obstacles
    MILP.constraints.obstacles_binary
    MILP.constraints.obstacles_corner_cutting
    MILP.constraints.sense
    MILP.constraints.abs_orient_diff
    MILP.constraints.orient_binary
    MILP.constraints.orient_increment
    ];

% Cost
MILP.BuildCost();

% Solve
options = sdpsettings('solver','gurobi');
%options.gurobi.TimeLimit = 180;
%options.gurobi.Heuristics = 0.025; % only fine-tuning required

%disp("Starting optimization");
timer = tic;
diagnostics = optimize(constraints,MILP.cost,options);
%disp("Optimization time: "+toc(timer));
elapsed_time = toc(timer);

data_processing = DataClass(MILP);
results_milp = data_processing.get_results();
results_milp.elapsed_time = elapsed_time;
results_milp.cost = value(MILP.cost);


end

