function [opti,opt_vars,cost] = set_opt(opti,pars,poly)

n_tar = length(poly.tar);
n_obs = length(poly.obs);
n_obs_sides = length(poly.obs{1}.q);
% Optimization variables

% Continuous
opt_vars.RX = opti.variable(pars.max_hor); % Position on x-axis
opt_vars.RY = opti.variable(pars.max_hor); % Position on y-axis
opt_vars.SIGMA = opti.variable(pars.max_hor-1); % Linear displacement (sort of an input)
opt_vars.KSI = opti.variable(pars.max_hor); % Linear velocity
opt_vars.ACC = opti.variable(pars.max_hor-1) ; % Linear acceleration (input)
opt_vars.ABS_ACC = opti.variable(pars.max_hor-1) ; % Linear acceleration (input)
opt_vars.PSI = opti.variable(pars.max_hor); % Orientation
opt_vars.PSI_INC = opti.variable(pars.max_hor-1); % Orientation increment (input)
opt_vars.ABS_PSI_INC = opti.variable(pars.max_hor-1); % Orientation increment (input)

% Binaries
opt_vars.B_hor = opti.variable(pars.max_hor); % Horizon binaries
for t = 1 : n_tar; opt_vars.B_tar{t} = opti.variable(pars.max_hor); end % target binaries
for o = 1 : n_obs; opt_vars.B_obs{o} = opti.variable(n_obs_sides*pars.max_hor); end % obs binaries

n_states = 4;
n_aux_cont_vars = 2;  % ABS_ACC and ABS_PSI_INC
n_inputs = 3; % acceleration, linear displacement, orientation increment


% Set discrete vars and solver
discrete_var_map = [zeros(pars.max_hor*n_states + (pars.max_hor-1)*n_inputs + (pars.max_hor-1)*n_aux_cont_vars,1);  % continuous
    ones(pars.max_hor,1); % B_hor
    ones(n_tar*pars.max_hor,1); % B_tar
    ones(n_obs*pars.max_hor*n_obs_sides,1) %B_obs
    ];

opts.discrete = discrete_var_map;
%opts.expand = true;
%opts2.time_limit = 180;
%opts2.solution_limit = 1;
%opts.expand = true;
% opts.verbose = true;
%opts2.mip_terminate = 1;
opts2.mip_maxtimereal = 600;
%opts2.ma_maxtimereal = 10;
%opts2.maxtime_real = 10;
%opts2.mip_opt_gap_rel = 20;
%opts2.mip_opt_gap_abs = 20;
opts2.mip_multistart=1;
%opti.solver('bonmin', opts,opts2);
opti.solver('knitro', opts,opts2);
% Cost
%cost = [1:pars.max_hor]*opt_vars.B_hor + 0.1*reshape(opt_vars.ACC,1,pars.max_hor-1)*opt_vars.ACC;
% acc_norm1 = [];
% for z = 1 : length(opt_vars.ABS_ACC)
%     acc_norm1 = [acc_norm1, norm(opt_vars.ACC(z),1)];
% end
%     
%cost = [1:pars.max_hor]*opt_vars.B_hor + 0.1*sum(acc_norm1) + 0.1*sum(opt_vars.ABS_PSI_INC);
cost = [1:pars.max_hor]*opt_vars.B_hor + 0.1*sum(opt_vars.ABS_ACC) + 0.1*sum(opt_vars.ABS_PSI_INC);
opti.minimize(cost);

% Constraints

    %% Absolute values constraints (1-norm not working on casadi)
    opti.subject_to(-opt_vars.ABS_ACC <= opt_vars.ACC <= opt_vars.ABS_ACC); 
    opti.subject_to(-opt_vars.ABS_PSI_INC <= opt_vars.PSI_INC  <= opt_vars.ABS_PSI_INC);

    %% Initial conditions
    opti.subject_to(opt_vars.RX(1) == pars.rx0+1.0000e-06)
    opti.subject_to(opt_vars.RY(1) == pars.ry0+1.0000e-06)
    opti.subject_to(opt_vars.RX(1) == pars.rx0+1.0000e-06)
    opti.subject_to(opt_vars.RY(1) == pars.ry0+1.0000e-06)
    opti.subject_to(opt_vars.KSI(1) == pars.ksi0+1.0000e-06)

    %% Dynamics constraints
    for k = 1 : pars.max_hor-1
        %  opt_vars.RX and opt_vars.RY
        opti.subject_to(opt_vars.RX(k+1) <= opt_vars.RX(k) + opt_vars.SIGMA(k)*cos(opt_vars.PSI(k)) + sum(opt_vars.B_hor(1:k-1))*pars.M);
        opti.subject_to(-opt_vars.RX(k+1) <= -opt_vars.RX(k) - opt_vars.SIGMA(k)*cos(opt_vars.PSI(k)) + sum(opt_vars.B_hor(1:k-1))*pars.M);
        opti.subject_to(opt_vars.RY(k+1) <= opt_vars.RY(k) + opt_vars.SIGMA(k)*sin(opt_vars.PSI(k)) + sum(opt_vars.B_hor(1:k-1))*pars.M);
        opti.subject_to(-opt_vars.RY(k+1) <= -opt_vars.RY(k) - opt_vars.SIGMA(k)*sin(opt_vars.PSI(k)) + sum(opt_vars.B_hor(1:k-1))*pars.M);

       % linear displacement
       opti.subject_to(opt_vars.SIGMA(k) <= opt_vars.KSI(k)*pars.T + opt_vars.ACC(k)*(pars.T^2/2) + sum(opt_vars.B_hor(1:k-1))*pars.M);
       opti.subject_to(-opt_vars.SIGMA(k) <= -opt_vars.KSI(k)*pars.T - opt_vars.ACC(k)*(pars.T^2/2) + sum(opt_vars.B_hor(1:k-1))*pars.M);

       % linear velocity
       opti.subject_to(opt_vars.KSI(k+1) <= opt_vars.KSI(k) + opt_vars.ACC(k)*pars.T + sum(opt_vars.B_hor(1:k-1))*pars.M);
       opti.subject_to(-opt_vars.KSI(k+1) <= -opt_vars.KSI(k) - opt_vars.ACC(k)*pars.T + sum(opt_vars.B_hor(1:k-1))*pars.M);

       % orientation
       opti.subject_to(opt_vars.PSI(k+1) <= opt_vars.PSI(k) + opt_vars.PSI_INC(k)*pars.T + sum(opt_vars.B_hor(1:k-1))*pars.M);
       opti.subject_to(-opt_vars.PSI(k+1) <= -opt_vars.PSI(k) - opt_vars.PSI_INC(k)*pars.T + sum(opt_vars.B_hor(1:k-1))*pars.M);
    end
    %% State and input bounds
     opti.subject_to(opt_vars.PSI(1) <= pars.ori_max + sum(opt_vars.B_hor(1:k-1))*pars.M);
     opti.subject_to(-opt_vars.PSI(1) <= -pars.ori_min + sum(opt_vars.B_hor(1:k-1))*pars.M);
     for k = 1 : pars.max_hor
        opti.subject_to(opt_vars.RX(k) <= pars.rx_max + sum(opt_vars.B_hor(1:k-1))*pars.M);
        opti.subject_to(-opt_vars.RX(k) <= -pars.rx_min + sum(opt_vars.B_hor(1:k-1))*pars.M);
        opti.subject_to(opt_vars.RY(k) <= pars.ry_max + sum(opt_vars.B_hor(1:k-1))*pars.M);
        opti.subject_to(-opt_vars.RY(k) <= -pars.ry_min + sum(opt_vars.B_hor(1:k-1))*pars.M);
        opti.subject_to(opt_vars.KSI(k) <= pars.lin_vel_max + sum(opt_vars.B_hor(1:k-1))*pars.M);
        opti.subject_to(-opt_vars.KSI(k) <= -pars.lin_vel_min + sum(opt_vars.B_hor(1:k-1))*pars.M);
%         opti.subject_to(opt_vars.PSI(k) <= pars.ori_max + sum(opt_vars.B_hor(1:k-1))*pars.M);
%         opti.subject_to(-opt_vars.PSI(k) <= -pars.ori_min + sum(opt_vars.B_hor(1:k-1))*pars.M);

        if k < pars.max_hor
             opti.subject_to(opt_vars.PSI_INC(k) <= pars.ori_inc_max + sum(opt_vars.B_hor(1:k))*pars.M);
             opti.subject_to(-opt_vars.PSI_INC(k) <= -pars.ori_inc_min + sum(opt_vars.B_hor(1:k))*pars.M);
            opti.subject_to(opt_vars.ACC(k) <= pars.lin_acc_max + sum(opt_vars.B_hor(1:k))*pars.M);
            opti.subject_to(-opt_vars.ACC(k) <= -pars.lin_acc_min + sum(opt_vars.B_hor(1:k))*pars.M);
        end
     end
     
     %% Target (variable horizon)
    sum_B = 0;
    for k = 1 : pars.max_hor
        opti.subject_to(0  <= opt_vars.B_hor(k) <= 1);
        pos = [opt_vars.RX(k);opt_vars.RY(k)];
        for t = 1 : n_tar
            opti.subject_to(0 <= opt_vars.B_tar{t}(k) <= 1);
            P = poly.tar{t}.P;
            q = poly.tar{t}.q;
            opti.subject_to(P * pos <= q + ones(length(q),1)*(1-opt_vars.B_tar{t}(k))*pars.M);
            
            % to visit all targets
            opti.subject_to(sum(opt_vars.B_tar{t}(1:k)) >= opt_vars.B_hor(k));
            
            % Avoid visiting targets multiple
            opti.subject_to(sum(opt_vars.B_tar{t})==1) 
        end
    end
    opti.subject_to(sum(opt_vars.B_hor) == 1);
    
    %% Obstacle avoidance
    
    for k = 1 : pars.max_hor
        pos = [opt_vars.RX(k);opt_vars.RY(k)];
        for o = 1 : n_obs
            P = poly.obs{o}.P;
            q = poly.obs{o}.q;
            
            obs_idx_k = (k-1)*n_obs_sides+1:k*n_obs_sides;
            
            opti.subject_to(0*ones(n_obs_sides,1) <= reshape(opt_vars.B_obs{o}(obs_idx_k),4,1) <= ones(n_obs_sides,1));
            opti.subject_to(-P * pos <= -q + (ones(length(q),1)-reshape(opt_vars.B_obs{o}(obs_idx_k),4,1))*pars.M + sum(opt_vars.B_hor(1:k-1))*pars.M);
            opti.subject_to(sum(opt_vars.B_obs{o}(obs_idx_k)) >= 1)

            if k < pars.max_hor
                next_pos = [opt_vars.RX(k+1);opt_vars.RY(k+1)];
                opti.subject_to(-P * next_pos <= -q + (ones(length(q),1)-reshape(opt_vars.B_obs{o}(obs_idx_k),4,1))*pars.M + sum(opt_vars.B_hor(1:k))*pars.M);
            end            
        end
        

        
        
    end
    
    
    

end

