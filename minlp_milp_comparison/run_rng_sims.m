clearvars; close all;

import casadi.*

% Preliminaries
opti = casadi.Opti(); % create optimization
pars = get_parameters();

params.number_obs_sides = 4;
params.number_obstacles = 2;
params.number_agents = 1;

n_sols = 1;
desired_sols = 50;

while n_sols <= desired_sols
    %disp("N_SOLS:" + string(trial));
    opti = casadi.Opti(); % create optimization
    scen = GenRngScenObsSide(params);
    poly = scen.polytopes;
    pars.rx0 = scen.rng_agent_pos(1);
    pars.ry0 = scen.rng_agent_pos(2);
    [opti,opt_vars,cost] = set_opt(opti,pars,poly); % set vars, const, and cost
    
    t0 = tic;
    sol = "unfeasible";
    try % this try catch handles unfeasibilty
        sol = opti.solve_limited();
    catch
    end
    
    % Check if there is feasible solution
    %if string(sol.stats().return_status) ~= "LIMIT_EXCEEDED"
    if sol ~= "unfeasible" % && sol.value(cost) > 0
        results_minlp.B_hor = opti.value(opt_vars.B_hor); N_opt = find(results_minlp.B_hor==1);
        minlp_elapsed_time= toc(t0);
        disp("FEASIBLE SOLUTION FOUND WITHIN TIME LIMITS");
        disp("SOLVE TIME: " + string(minlp_elapsed_time));
        
        % Get and save data
        results_minlp = get_minlp_results(opti,opt_vars);
        
        % trial = 1;
        
        
        % Solve MILP approximation for same environment
        results_milp.deg30 = solve_milp_approx(pars,poly,12);
        results_milp.deg15 = solve_milp_approx(pars,poly,24);
        results_milp.deg10 = solve_milp_approx(pars,poly,36);
        
        save_data(results_minlp,results_milp,poly,pars,minlp_elapsed_time,sol.value(cost),n_sols);
        n_sols = n_sols + 1;
    elseif sol == "unfeasible"
        disp("NO FEASIBLE SOLUTION");
    elseif sol.value(cost) < 1
            time_limit_case = time_limit_case + 1;

     
    end
    

    
    
end

%%
close all; clc;


plot_comparisons();




